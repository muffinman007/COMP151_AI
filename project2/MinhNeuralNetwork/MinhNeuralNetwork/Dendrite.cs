﻿/***
 * Full credits goes to the orginal author that is teaching this:
 * https://social.technet.microsoft.com/wiki/contents/articles/36428.basis-of-neural-networks-in-c.aspx#Scope
 * Above is the website that I've used to learn backpropagation
 * 
 * This class mimic the dendrite ending on the neuron cells. 
 * the class Dendrite represent a single input
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhNeuralNetwork {

	/***
	 * Represent a single input
	 */
	 [Serializable]
	class Dendrite {

		// the website tutorial use a public property
		// so I use a public field.
		// the weight is a value between (0, 1) 
		public Double weight;

		public Dendrite() {
			this.weight = TestSimpleRNG.SimpleRNG.GetUniform();		
		}

	}
}
