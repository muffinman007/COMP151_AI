﻿namespace MinhNeuralNetwork {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.loadFileBtn = new System.Windows.Forms.Button();
			this.outputTxtBox = new System.Windows.Forms.TextBox();
			this.nudLearningRate = new System.Windows.Forms.NumericUpDown();
			this.nudEpoch = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.runTrainingBtn = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.outputQueryLabel = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.africaTB = new System.Windows.Forms.TextBox();
			this.americaTB = new System.Windows.Forms.TextBox();
			this.antarcticaTB = new System.Windows.Forms.TextBox();
			this.arcticTB = new System.Windows.Forms.TextBox();
			this.asiaTB = new System.Windows.Forms.TextBox();
			this.atlanticTB = new System.Windows.Forms.TextBox();
			this.australiaTB = new System.Windows.Forms.TextBox();
			this.europeTB = new System.Windows.Forms.TextBox();
			this.indianTB = new System.Windows.Forms.TextBox();
			this.pacificTB = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.showOutputCB = new System.Windows.Forms.CheckBox();
			this.loadTestFileBtn = new System.Windows.Forms.Button();
			this.loadInBinaryObj = new System.Windows.Forms.Button();
			this.runTestBtn = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label17 = new System.Windows.Forms.Label();
			this.label18 = new System.Windows.Forms.Label();
			this.label19 = new System.Windows.Forms.Label();
			this.label20 = new System.Windows.Forms.Label();
			this.label21 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label25 = new System.Windows.Forms.Label();
			this.label26 = new System.Windows.Forms.Label();
			this.label27 = new System.Windows.Forms.Label();
			this.label28 = new System.Windows.Forms.Label();
			this.label29 = new System.Windows.Forms.Label();
			this.label30 = new System.Windows.Forms.Label();
			this.label31 = new System.Windows.Forms.Label();
			this.africaCorrectTB = new System.Windows.Forms.TextBox();
			this.afrTruPosTB = new System.Windows.Forms.TextBox();
			this.afrTruNegTB = new System.Windows.Forms.TextBox();
			this.afrFalPosTB = new System.Windows.Forms.TextBox();
			this.afrFalNegTB = new System.Windows.Forms.TextBox();
			this.ameFalNegTB = new System.Windows.Forms.TextBox();
			this.ameFalPosTB = new System.Windows.Forms.TextBox();
			this.ameTruNegTB = new System.Windows.Forms.TextBox();
			this.ameTruPosTB = new System.Windows.Forms.TextBox();
			this.ameCorTB = new System.Windows.Forms.TextBox();
			this.antFalNegTB = new System.Windows.Forms.TextBox();
			this.antFalPosTB = new System.Windows.Forms.TextBox();
			this.antTruNegTB = new System.Windows.Forms.TextBox();
			this.antTruPosTB = new System.Windows.Forms.TextBox();
			this.antCorTB = new System.Windows.Forms.TextBox();
			this.arcFalNegTB = new System.Windows.Forms.TextBox();
			this.arcFalPosTB = new System.Windows.Forms.TextBox();
			this.arcTruNegTB = new System.Windows.Forms.TextBox();
			this.arcTruPosTB = new System.Windows.Forms.TextBox();
			this.arcCorTB = new System.Windows.Forms.TextBox();
			this.asiFalNegTB = new System.Windows.Forms.TextBox();
			this.asiFalPosTB = new System.Windows.Forms.TextBox();
			this.asiTruNegTB = new System.Windows.Forms.TextBox();
			this.asiTruPosTB = new System.Windows.Forms.TextBox();
			this.asiCorTB = new System.Windows.Forms.TextBox();
			this.atlFalNegTB = new System.Windows.Forms.TextBox();
			this.atlFalPosTB = new System.Windows.Forms.TextBox();
			this.atlTruNegTB = new System.Windows.Forms.TextBox();
			this.atlTruPosTB = new System.Windows.Forms.TextBox();
			this.atlCorTB = new System.Windows.Forms.TextBox();
			this.ausFalNegTB = new System.Windows.Forms.TextBox();
			this.ausFalPosTB = new System.Windows.Forms.TextBox();
			this.ausTruNegTB = new System.Windows.Forms.TextBox();
			this.ausTruPosTB = new System.Windows.Forms.TextBox();
			this.ausCorTB = new System.Windows.Forms.TextBox();
			this.eurFalNegTB = new System.Windows.Forms.TextBox();
			this.eurFalPosTB = new System.Windows.Forms.TextBox();
			this.eurTruNegTB = new System.Windows.Forms.TextBox();
			this.eurTruPosTB = new System.Windows.Forms.TextBox();
			this.eurCorTB = new System.Windows.Forms.TextBox();
			this.indFalNegTB = new System.Windows.Forms.TextBox();
			this.indFalPosTB = new System.Windows.Forms.TextBox();
			this.indTruNegTB = new System.Windows.Forms.TextBox();
			this.indTruPosTB = new System.Windows.Forms.TextBox();
			this.indCorTB = new System.Windows.Forms.TextBox();
			this.pacFalNegTB = new System.Windows.Forms.TextBox();
			this.pacFalPosTB = new System.Windows.Forms.TextBox();
			this.pacTruNegTB = new System.Windows.Forms.TextBox();
			this.pacTruPosTB = new System.Windows.Forms.TextBox();
			this.pacCorTB = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.nudLearningRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nudEpoch)).BeginInit();
			this.SuspendLayout();
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// loadFileBtn
			// 
			this.loadFileBtn.Location = new System.Drawing.Point(15, 50);
			this.loadFileBtn.Name = "loadFileBtn";
			this.loadFileBtn.Size = new System.Drawing.Size(132, 23);
			this.loadFileBtn.TabIndex = 0;
			this.loadFileBtn.Text = "Load In Training File";
			this.loadFileBtn.UseVisualStyleBackColor = true;
			this.loadFileBtn.Click += new System.EventHandler(this.loadFileBtn_Click);
			// 
			// outputTxtBox
			// 
			this.outputTxtBox.BackColor = System.Drawing.Color.White;
			this.outputTxtBox.Location = new System.Drawing.Point(257, 12);
			this.outputTxtBox.Multiline = true;
			this.outputTxtBox.Name = "outputTxtBox";
			this.outputTxtBox.ReadOnly = true;
			this.outputTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.outputTxtBox.Size = new System.Drawing.Size(211, 493);
			this.outputTxtBox.TabIndex = 1;
			this.outputTxtBox.Text = "Please load in training data file.";
			// 
			// nudLearningRate
			// 
			this.nudLearningRate.DecimalPlaces = 7;
			this.nudLearningRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            458752});
			this.nudLearningRate.Location = new System.Drawing.Point(131, 88);
			this.nudLearningRate.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.nudLearningRate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            458752});
			this.nudLearningRate.Name = "nudLearningRate";
			this.nudLearningRate.Size = new System.Drawing.Size(120, 20);
			this.nudLearningRate.TabIndex = 4;
			this.nudLearningRate.Value = new decimal(new int[] {
            1,
            0,
            0,
            458752});
			// 
			// nudEpoch
			// 
			this.nudEpoch.Location = new System.Drawing.Point(131, 125);
			this.nudEpoch.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.nudEpoch.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nudEpoch.Name = "nudEpoch";
			this.nudEpoch.Size = new System.Drawing.Size(120, 20);
			this.nudEpoch.TabIndex = 5;
			this.nudEpoch.Value = new decimal(new int[] {
            30000,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(12, 13);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(89, 17);
			this.label1.TabIndex = 6;
			this.label1.Text = "Train Brain";
			// 
			// runTrainingBtn
			// 
			this.runTrainingBtn.Enabled = false;
			this.runTrainingBtn.Location = new System.Drawing.Point(160, 50);
			this.runTrainingBtn.Name = "runTrainingBtn";
			this.runTrainingBtn.Size = new System.Drawing.Size(91, 23);
			this.runTrainingBtn.TabIndex = 9;
			this.runTrainingBtn.Text = "Run Training";
			this.runTrainingBtn.UseVisualStyleBackColor = true;
			this.runTrainingBtn.Click += new System.EventHandler(this.runTrainingBtn_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(13, 90);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(72, 13);
			this.label4.TabIndex = 11;
			this.label4.Text = "Learning rate:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(13, 127);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(112, 13);
			this.label5.TabIndex = 12;
			this.label5.Text = "Epoch - training cycle:";
			// 
			// label6
			// 
			this.label6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.label6.Location = new System.Drawing.Point(12, 164);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(239, 1);
			this.label6.TabIndex = 13;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(485, 13);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(83, 17);
			this.label3.TabIndex = 14;
			this.label3.Text = "Test Brain";
			// 
			// outputQueryLabel
			// 
			this.outputQueryLabel.AutoSize = true;
			this.outputQueryLabel.Location = new System.Drawing.Point(12, 254);
			this.outputQueryLabel.Name = "outputQueryLabel";
			this.outputQueryLabel.Size = new System.Drawing.Size(34, 13);
			this.outputQueryLabel.TabIndex = 15;
			this.outputQueryLabel.Text = "Africa";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(12, 185);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(103, 13);
			this.label8.TabIndex = 16;
			this.label8.Text = "Output Layer Values";
			// 
			// africaTB
			// 
			this.africaTB.BackColor = System.Drawing.Color.White;
			this.africaTB.Enabled = false;
			this.africaTB.Location = new System.Drawing.Point(106, 251);
			this.africaTB.Name = "africaTB";
			this.africaTB.ReadOnly = true;
			this.africaTB.Size = new System.Drawing.Size(145, 20);
			this.africaTB.TabIndex = 19;
			// 
			// americaTB
			// 
			this.americaTB.BackColor = System.Drawing.Color.White;
			this.americaTB.Enabled = false;
			this.americaTB.Location = new System.Drawing.Point(106, 277);
			this.americaTB.Name = "americaTB";
			this.americaTB.ReadOnly = true;
			this.americaTB.Size = new System.Drawing.Size(145, 20);
			this.americaTB.TabIndex = 21;
			// 
			// antarcticaTB
			// 
			this.antarcticaTB.BackColor = System.Drawing.Color.White;
			this.antarcticaTB.Enabled = false;
			this.antarcticaTB.Location = new System.Drawing.Point(106, 303);
			this.antarcticaTB.Name = "antarcticaTB";
			this.antarcticaTB.ReadOnly = true;
			this.antarcticaTB.Size = new System.Drawing.Size(145, 20);
			this.antarcticaTB.TabIndex = 22;
			// 
			// arcticTB
			// 
			this.arcticTB.BackColor = System.Drawing.Color.White;
			this.arcticTB.Enabled = false;
			this.arcticTB.Location = new System.Drawing.Point(106, 329);
			this.arcticTB.Name = "arcticTB";
			this.arcticTB.ReadOnly = true;
			this.arcticTB.Size = new System.Drawing.Size(145, 20);
			this.arcticTB.TabIndex = 23;
			// 
			// asiaTB
			// 
			this.asiaTB.BackColor = System.Drawing.Color.White;
			this.asiaTB.Enabled = false;
			this.asiaTB.Location = new System.Drawing.Point(106, 355);
			this.asiaTB.Name = "asiaTB";
			this.asiaTB.ReadOnly = true;
			this.asiaTB.Size = new System.Drawing.Size(145, 20);
			this.asiaTB.TabIndex = 24;
			// 
			// atlanticTB
			// 
			this.atlanticTB.BackColor = System.Drawing.Color.White;
			this.atlanticTB.Enabled = false;
			this.atlanticTB.Location = new System.Drawing.Point(106, 381);
			this.atlanticTB.Name = "atlanticTB";
			this.atlanticTB.ReadOnly = true;
			this.atlanticTB.Size = new System.Drawing.Size(145, 20);
			this.atlanticTB.TabIndex = 25;
			// 
			// australiaTB
			// 
			this.australiaTB.BackColor = System.Drawing.Color.White;
			this.australiaTB.Enabled = false;
			this.australiaTB.Location = new System.Drawing.Point(106, 407);
			this.australiaTB.Name = "australiaTB";
			this.australiaTB.ReadOnly = true;
			this.australiaTB.Size = new System.Drawing.Size(145, 20);
			this.australiaTB.TabIndex = 26;
			// 
			// europeTB
			// 
			this.europeTB.BackColor = System.Drawing.Color.White;
			this.europeTB.Enabled = false;
			this.europeTB.Location = new System.Drawing.Point(106, 433);
			this.europeTB.Name = "europeTB";
			this.europeTB.ReadOnly = true;
			this.europeTB.Size = new System.Drawing.Size(145, 20);
			this.europeTB.TabIndex = 27;
			// 
			// indianTB
			// 
			this.indianTB.BackColor = System.Drawing.Color.White;
			this.indianTB.Enabled = false;
			this.indianTB.Location = new System.Drawing.Point(106, 459);
			this.indianTB.Name = "indianTB";
			this.indianTB.ReadOnly = true;
			this.indianTB.Size = new System.Drawing.Size(145, 20);
			this.indianTB.TabIndex = 28;
			// 
			// pacificTB
			// 
			this.pacificTB.BackColor = System.Drawing.Color.White;
			this.pacificTB.Enabled = false;
			this.pacificTB.Location = new System.Drawing.Point(106, 485);
			this.pacificTB.Name = "pacificTB";
			this.pacificTB.ReadOnly = true;
			this.pacificTB.Size = new System.Drawing.Size(145, 20);
			this.pacificTB.TabIndex = 29;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(13, 280);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(45, 13);
			this.label7.TabIndex = 30;
			this.label7.Text = "America";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(13, 306);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(55, 13);
			this.label9.TabIndex = 31;
			this.label9.Text = "Antarctica";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(13, 332);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(34, 13);
			this.label10.TabIndex = 32;
			this.label10.Text = "Arctic";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(13, 358);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(27, 13);
			this.label11.TabIndex = 33;
			this.label11.Text = "Asia";
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(12, 384);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(42, 13);
			this.label12.TabIndex = 34;
			this.label12.Text = "Atlantic";
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(13, 410);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(47, 13);
			this.label13.TabIndex = 35;
			this.label13.Text = "Australia";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(13, 436);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(41, 13);
			this.label14.TabIndex = 36;
			this.label14.Text = "Europe";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(13, 462);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(36, 13);
			this.label15.TabIndex = 37;
			this.label15.Text = "Indian";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(13, 488);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(39, 13);
			this.label16.TabIndex = 38;
			this.label16.Text = "Pacific";
			// 
			// showOutputCB
			// 
			this.showOutputCB.AutoSize = true;
			this.showOutputCB.Checked = true;
			this.showOutputCB.CheckState = System.Windows.Forms.CheckState.Checked;
			this.showOutputCB.Location = new System.Drawing.Point(15, 211);
			this.showOutputCB.Name = "showOutputCB";
			this.showOutputCB.Size = new System.Drawing.Size(152, 17);
			this.showOutputCB.TabIndex = 39;
			this.showOutputCB.Text = "Show Output Layer Values";
			this.showOutputCB.UseVisualStyleBackColor = true;
			// 
			// loadTestFileBtn
			// 
			this.loadTestFileBtn.Enabled = false;
			this.loadTestFileBtn.Location = new System.Drawing.Point(488, 33);
			this.loadTestFileBtn.Name = "loadTestFileBtn";
			this.loadTestFileBtn.Size = new System.Drawing.Size(113, 23);
			this.loadTestFileBtn.TabIndex = 40;
			this.loadTestFileBtn.Text = "Load In Test File";
			this.loadTestFileBtn.UseVisualStyleBackColor = true;
			this.loadTestFileBtn.Click += new System.EventHandler(this.loadTestFileBtn_Click);
			// 
			// loadInBinaryObj
			// 
			this.loadInBinaryObj.Location = new System.Drawing.Point(618, 33);
			this.loadInBinaryObj.Name = "loadInBinaryObj";
			this.loadInBinaryObj.Size = new System.Drawing.Size(170, 23);
			this.loadInBinaryObj.TabIndex = 41;
			this.loadInBinaryObj.Text = "Load In Binary Brain Object";
			this.loadInBinaryObj.UseVisualStyleBackColor = true;
			this.loadInBinaryObj.Click += new System.EventHandler(this.loadInBinaryObj_Click);
			// 
			// runTestBtn
			// 
			this.runTestBtn.Enabled = false;
			this.runTestBtn.Location = new System.Drawing.Point(808, 33);
			this.runTestBtn.Name = "runTestBtn";
			this.runTestBtn.Size = new System.Drawing.Size(93, 23);
			this.runTestBtn.TabIndex = 42;
			this.runTestBtn.Text = "Run Test";
			this.runTestBtn.UseVisualStyleBackColor = true;
			this.runTestBtn.Click += new System.EventHandler(this.runTestBtn_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(485, 90);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(42, 13);
			this.label2.TabIndex = 43;
			this.label2.Text = "Neuron";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(567, 90);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(44, 13);
			this.label17.TabIndex = 44;
			this.label17.Text = "Correct ";
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(665, 90);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(72, 13);
			this.label18.TabIndex = 45;
			this.label18.Text = "True Positive ";
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(763, 90);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(75, 13);
			this.label19.TabIndex = 46;
			this.label19.Text = "True Negative";
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(861, 90);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(75, 13);
			this.label20.TabIndex = 47;
			this.label20.Text = "False Positive ";
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(959, 90);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(78, 13);
			this.label21.TabIndex = 48;
			this.label21.Text = "False Negative";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(485, 438);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(39, 13);
			this.label22.TabIndex = 58;
			this.label22.Text = "Pacific";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(486, 404);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(36, 13);
			this.label23.TabIndex = 57;
			this.label23.Text = "Indian";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(486, 370);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(41, 13);
			this.label24.TabIndex = 56;
			this.label24.Text = "Europe";
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(486, 336);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(47, 13);
			this.label25.TabIndex = 55;
			this.label25.Text = "Australia";
			// 
			// label26
			// 
			this.label26.AutoSize = true;
			this.label26.Location = new System.Drawing.Point(486, 302);
			this.label26.Name = "label26";
			this.label26.Size = new System.Drawing.Size(42, 13);
			this.label26.TabIndex = 54;
			this.label26.Text = "Atlantic";
			// 
			// label27
			// 
			this.label27.AutoSize = true;
			this.label27.Location = new System.Drawing.Point(486, 268);
			this.label27.Name = "label27";
			this.label27.Size = new System.Drawing.Size(27, 13);
			this.label27.TabIndex = 53;
			this.label27.Text = "Asia";
			// 
			// label28
			// 
			this.label28.AutoSize = true;
			this.label28.Location = new System.Drawing.Point(486, 234);
			this.label28.Name = "label28";
			this.label28.Size = new System.Drawing.Size(34, 13);
			this.label28.TabIndex = 52;
			this.label28.Text = "Arctic";
			// 
			// label29
			// 
			this.label29.AutoSize = true;
			this.label29.Location = new System.Drawing.Point(486, 200);
			this.label29.Name = "label29";
			this.label29.Size = new System.Drawing.Size(55, 13);
			this.label29.TabIndex = 51;
			this.label29.Text = "Antarctica";
			// 
			// label30
			// 
			this.label30.AutoSize = true;
			this.label30.Location = new System.Drawing.Point(486, 166);
			this.label30.Name = "label30";
			this.label30.Size = new System.Drawing.Size(45, 13);
			this.label30.TabIndex = 50;
			this.label30.Text = "America";
			// 
			// label31
			// 
			this.label31.AutoSize = true;
			this.label31.Location = new System.Drawing.Point(485, 132);
			this.label31.Name = "label31";
			this.label31.Size = new System.Drawing.Size(34, 13);
			this.label31.TabIndex = 49;
			this.label31.Text = "Africa";
			// 
			// africaCorrectTB
			// 
			this.africaCorrectTB.BackColor = System.Drawing.Color.White;
			this.africaCorrectTB.Location = new System.Drawing.Point(570, 129);
			this.africaCorrectTB.Name = "africaCorrectTB";
			this.africaCorrectTB.ReadOnly = true;
			this.africaCorrectTB.Size = new System.Drawing.Size(82, 20);
			this.africaCorrectTB.TabIndex = 59;
			// 
			// afrTruPosTB
			// 
			this.afrTruPosTB.BackColor = System.Drawing.Color.White;
			this.afrTruPosTB.Location = new System.Drawing.Point(668, 129);
			this.afrTruPosTB.Name = "afrTruPosTB";
			this.afrTruPosTB.ReadOnly = true;
			this.afrTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.afrTruPosTB.TabIndex = 60;
			// 
			// afrTruNegTB
			// 
			this.afrTruNegTB.BackColor = System.Drawing.Color.White;
			this.afrTruNegTB.Location = new System.Drawing.Point(766, 129);
			this.afrTruNegTB.Name = "afrTruNegTB";
			this.afrTruNegTB.ReadOnly = true;
			this.afrTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.afrTruNegTB.TabIndex = 61;
			// 
			// afrFalPosTB
			// 
			this.afrFalPosTB.BackColor = System.Drawing.Color.White;
			this.afrFalPosTB.Location = new System.Drawing.Point(864, 129);
			this.afrFalPosTB.Name = "afrFalPosTB";
			this.afrFalPosTB.ReadOnly = true;
			this.afrFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.afrFalPosTB.TabIndex = 62;
			// 
			// afrFalNegTB
			// 
			this.afrFalNegTB.BackColor = System.Drawing.Color.White;
			this.afrFalNegTB.Location = new System.Drawing.Point(962, 129);
			this.afrFalNegTB.Name = "afrFalNegTB";
			this.afrFalNegTB.ReadOnly = true;
			this.afrFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.afrFalNegTB.TabIndex = 63;
			// 
			// ameFalNegTB
			// 
			this.ameFalNegTB.BackColor = System.Drawing.Color.White;
			this.ameFalNegTB.Location = new System.Drawing.Point(962, 163);
			this.ameFalNegTB.Name = "ameFalNegTB";
			this.ameFalNegTB.ReadOnly = true;
			this.ameFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.ameFalNegTB.TabIndex = 68;
			// 
			// ameFalPosTB
			// 
			this.ameFalPosTB.BackColor = System.Drawing.Color.White;
			this.ameFalPosTB.Location = new System.Drawing.Point(864, 163);
			this.ameFalPosTB.Name = "ameFalPosTB";
			this.ameFalPosTB.ReadOnly = true;
			this.ameFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.ameFalPosTB.TabIndex = 67;
			// 
			// ameTruNegTB
			// 
			this.ameTruNegTB.BackColor = System.Drawing.Color.White;
			this.ameTruNegTB.Location = new System.Drawing.Point(766, 163);
			this.ameTruNegTB.Name = "ameTruNegTB";
			this.ameTruNegTB.ReadOnly = true;
			this.ameTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.ameTruNegTB.TabIndex = 66;
			// 
			// ameTruPosTB
			// 
			this.ameTruPosTB.BackColor = System.Drawing.Color.White;
			this.ameTruPosTB.Location = new System.Drawing.Point(668, 163);
			this.ameTruPosTB.Name = "ameTruPosTB";
			this.ameTruPosTB.ReadOnly = true;
			this.ameTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.ameTruPosTB.TabIndex = 65;
			// 
			// ameCorTB
			// 
			this.ameCorTB.BackColor = System.Drawing.Color.White;
			this.ameCorTB.Location = new System.Drawing.Point(570, 163);
			this.ameCorTB.Name = "ameCorTB";
			this.ameCorTB.ReadOnly = true;
			this.ameCorTB.Size = new System.Drawing.Size(82, 20);
			this.ameCorTB.TabIndex = 64;
			// 
			// antFalNegTB
			// 
			this.antFalNegTB.BackColor = System.Drawing.Color.White;
			this.antFalNegTB.Location = new System.Drawing.Point(962, 197);
			this.antFalNegTB.Name = "antFalNegTB";
			this.antFalNegTB.ReadOnly = true;
			this.antFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.antFalNegTB.TabIndex = 73;
			// 
			// antFalPosTB
			// 
			this.antFalPosTB.BackColor = System.Drawing.Color.White;
			this.antFalPosTB.Location = new System.Drawing.Point(864, 197);
			this.antFalPosTB.Name = "antFalPosTB";
			this.antFalPosTB.ReadOnly = true;
			this.antFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.antFalPosTB.TabIndex = 72;
			// 
			// antTruNegTB
			// 
			this.antTruNegTB.BackColor = System.Drawing.Color.White;
			this.antTruNegTB.Location = new System.Drawing.Point(766, 197);
			this.antTruNegTB.Name = "antTruNegTB";
			this.antTruNegTB.ReadOnly = true;
			this.antTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.antTruNegTB.TabIndex = 71;
			// 
			// antTruPosTB
			// 
			this.antTruPosTB.BackColor = System.Drawing.Color.White;
			this.antTruPosTB.Location = new System.Drawing.Point(668, 197);
			this.antTruPosTB.Name = "antTruPosTB";
			this.antTruPosTB.ReadOnly = true;
			this.antTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.antTruPosTB.TabIndex = 70;
			// 
			// antCorTB
			// 
			this.antCorTB.BackColor = System.Drawing.Color.White;
			this.antCorTB.Location = new System.Drawing.Point(570, 197);
			this.antCorTB.Name = "antCorTB";
			this.antCorTB.ReadOnly = true;
			this.antCorTB.Size = new System.Drawing.Size(82, 20);
			this.antCorTB.TabIndex = 69;
			// 
			// arcFalNegTB
			// 
			this.arcFalNegTB.BackColor = System.Drawing.Color.White;
			this.arcFalNegTB.Location = new System.Drawing.Point(962, 231);
			this.arcFalNegTB.Name = "arcFalNegTB";
			this.arcFalNegTB.ReadOnly = true;
			this.arcFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.arcFalNegTB.TabIndex = 78;
			// 
			// arcFalPosTB
			// 
			this.arcFalPosTB.BackColor = System.Drawing.Color.White;
			this.arcFalPosTB.Location = new System.Drawing.Point(864, 231);
			this.arcFalPosTB.Name = "arcFalPosTB";
			this.arcFalPosTB.ReadOnly = true;
			this.arcFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.arcFalPosTB.TabIndex = 77;
			// 
			// arcTruNegTB
			// 
			this.arcTruNegTB.BackColor = System.Drawing.Color.White;
			this.arcTruNegTB.Location = new System.Drawing.Point(766, 231);
			this.arcTruNegTB.Name = "arcTruNegTB";
			this.arcTruNegTB.ReadOnly = true;
			this.arcTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.arcTruNegTB.TabIndex = 76;
			// 
			// arcTruPosTB
			// 
			this.arcTruPosTB.BackColor = System.Drawing.Color.White;
			this.arcTruPosTB.Location = new System.Drawing.Point(668, 231);
			this.arcTruPosTB.Name = "arcTruPosTB";
			this.arcTruPosTB.ReadOnly = true;
			this.arcTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.arcTruPosTB.TabIndex = 75;
			// 
			// arcCorTB
			// 
			this.arcCorTB.BackColor = System.Drawing.Color.White;
			this.arcCorTB.Location = new System.Drawing.Point(570, 231);
			this.arcCorTB.Name = "arcCorTB";
			this.arcCorTB.ReadOnly = true;
			this.arcCorTB.Size = new System.Drawing.Size(82, 20);
			this.arcCorTB.TabIndex = 74;
			// 
			// asiFalNegTB
			// 
			this.asiFalNegTB.BackColor = System.Drawing.Color.White;
			this.asiFalNegTB.Location = new System.Drawing.Point(962, 265);
			this.asiFalNegTB.Name = "asiFalNegTB";
			this.asiFalNegTB.ReadOnly = true;
			this.asiFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.asiFalNegTB.TabIndex = 83;
			// 
			// asiFalPosTB
			// 
			this.asiFalPosTB.BackColor = System.Drawing.Color.White;
			this.asiFalPosTB.Location = new System.Drawing.Point(864, 265);
			this.asiFalPosTB.Name = "asiFalPosTB";
			this.asiFalPosTB.ReadOnly = true;
			this.asiFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.asiFalPosTB.TabIndex = 82;
			// 
			// asiTruNegTB
			// 
			this.asiTruNegTB.BackColor = System.Drawing.Color.White;
			this.asiTruNegTB.Location = new System.Drawing.Point(766, 265);
			this.asiTruNegTB.Name = "asiTruNegTB";
			this.asiTruNegTB.ReadOnly = true;
			this.asiTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.asiTruNegTB.TabIndex = 81;
			// 
			// asiTruPosTB
			// 
			this.asiTruPosTB.BackColor = System.Drawing.Color.White;
			this.asiTruPosTB.Location = new System.Drawing.Point(668, 265);
			this.asiTruPosTB.Name = "asiTruPosTB";
			this.asiTruPosTB.ReadOnly = true;
			this.asiTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.asiTruPosTB.TabIndex = 80;
			// 
			// asiCorTB
			// 
			this.asiCorTB.BackColor = System.Drawing.Color.White;
			this.asiCorTB.Location = new System.Drawing.Point(570, 265);
			this.asiCorTB.Name = "asiCorTB";
			this.asiCorTB.ReadOnly = true;
			this.asiCorTB.Size = new System.Drawing.Size(82, 20);
			this.asiCorTB.TabIndex = 79;
			// 
			// atlFalNegTB
			// 
			this.atlFalNegTB.BackColor = System.Drawing.Color.White;
			this.atlFalNegTB.Location = new System.Drawing.Point(962, 299);
			this.atlFalNegTB.Name = "atlFalNegTB";
			this.atlFalNegTB.ReadOnly = true;
			this.atlFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.atlFalNegTB.TabIndex = 88;
			// 
			// atlFalPosTB
			// 
			this.atlFalPosTB.BackColor = System.Drawing.Color.White;
			this.atlFalPosTB.Location = new System.Drawing.Point(864, 299);
			this.atlFalPosTB.Name = "atlFalPosTB";
			this.atlFalPosTB.ReadOnly = true;
			this.atlFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.atlFalPosTB.TabIndex = 87;
			// 
			// atlTruNegTB
			// 
			this.atlTruNegTB.BackColor = System.Drawing.Color.White;
			this.atlTruNegTB.Location = new System.Drawing.Point(766, 299);
			this.atlTruNegTB.Name = "atlTruNegTB";
			this.atlTruNegTB.ReadOnly = true;
			this.atlTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.atlTruNegTB.TabIndex = 86;
			// 
			// atlTruPosTB
			// 
			this.atlTruPosTB.BackColor = System.Drawing.Color.White;
			this.atlTruPosTB.Location = new System.Drawing.Point(668, 299);
			this.atlTruPosTB.Name = "atlTruPosTB";
			this.atlTruPosTB.ReadOnly = true;
			this.atlTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.atlTruPosTB.TabIndex = 85;
			// 
			// atlCorTB
			// 
			this.atlCorTB.BackColor = System.Drawing.Color.White;
			this.atlCorTB.Location = new System.Drawing.Point(570, 299);
			this.atlCorTB.Name = "atlCorTB";
			this.atlCorTB.ReadOnly = true;
			this.atlCorTB.Size = new System.Drawing.Size(82, 20);
			this.atlCorTB.TabIndex = 84;
			// 
			// ausFalNegTB
			// 
			this.ausFalNegTB.BackColor = System.Drawing.Color.White;
			this.ausFalNegTB.Location = new System.Drawing.Point(962, 333);
			this.ausFalNegTB.Name = "ausFalNegTB";
			this.ausFalNegTB.ReadOnly = true;
			this.ausFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.ausFalNegTB.TabIndex = 93;
			// 
			// ausFalPosTB
			// 
			this.ausFalPosTB.BackColor = System.Drawing.Color.White;
			this.ausFalPosTB.Location = new System.Drawing.Point(864, 333);
			this.ausFalPosTB.Name = "ausFalPosTB";
			this.ausFalPosTB.ReadOnly = true;
			this.ausFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.ausFalPosTB.TabIndex = 92;
			// 
			// ausTruNegTB
			// 
			this.ausTruNegTB.BackColor = System.Drawing.Color.White;
			this.ausTruNegTB.Location = new System.Drawing.Point(766, 333);
			this.ausTruNegTB.Name = "ausTruNegTB";
			this.ausTruNegTB.ReadOnly = true;
			this.ausTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.ausTruNegTB.TabIndex = 91;
			// 
			// ausTruPosTB
			// 
			this.ausTruPosTB.BackColor = System.Drawing.Color.White;
			this.ausTruPosTB.Location = new System.Drawing.Point(668, 333);
			this.ausTruPosTB.Name = "ausTruPosTB";
			this.ausTruPosTB.ReadOnly = true;
			this.ausTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.ausTruPosTB.TabIndex = 90;
			// 
			// ausCorTB
			// 
			this.ausCorTB.BackColor = System.Drawing.Color.White;
			this.ausCorTB.Location = new System.Drawing.Point(570, 333);
			this.ausCorTB.Name = "ausCorTB";
			this.ausCorTB.ReadOnly = true;
			this.ausCorTB.Size = new System.Drawing.Size(82, 20);
			this.ausCorTB.TabIndex = 89;
			// 
			// eurFalNegTB
			// 
			this.eurFalNegTB.BackColor = System.Drawing.Color.White;
			this.eurFalNegTB.Location = new System.Drawing.Point(962, 367);
			this.eurFalNegTB.Name = "eurFalNegTB";
			this.eurFalNegTB.ReadOnly = true;
			this.eurFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.eurFalNegTB.TabIndex = 98;
			// 
			// eurFalPosTB
			// 
			this.eurFalPosTB.BackColor = System.Drawing.Color.White;
			this.eurFalPosTB.Location = new System.Drawing.Point(864, 367);
			this.eurFalPosTB.Name = "eurFalPosTB";
			this.eurFalPosTB.ReadOnly = true;
			this.eurFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.eurFalPosTB.TabIndex = 97;
			// 
			// eurTruNegTB
			// 
			this.eurTruNegTB.BackColor = System.Drawing.Color.White;
			this.eurTruNegTB.Location = new System.Drawing.Point(766, 367);
			this.eurTruNegTB.Name = "eurTruNegTB";
			this.eurTruNegTB.ReadOnly = true;
			this.eurTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.eurTruNegTB.TabIndex = 96;
			// 
			// eurTruPosTB
			// 
			this.eurTruPosTB.BackColor = System.Drawing.Color.White;
			this.eurTruPosTB.Location = new System.Drawing.Point(668, 367);
			this.eurTruPosTB.Name = "eurTruPosTB";
			this.eurTruPosTB.ReadOnly = true;
			this.eurTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.eurTruPosTB.TabIndex = 95;
			// 
			// eurCorTB
			// 
			this.eurCorTB.BackColor = System.Drawing.Color.White;
			this.eurCorTB.Location = new System.Drawing.Point(570, 367);
			this.eurCorTB.Name = "eurCorTB";
			this.eurCorTB.ReadOnly = true;
			this.eurCorTB.Size = new System.Drawing.Size(82, 20);
			this.eurCorTB.TabIndex = 94;
			// 
			// indFalNegTB
			// 
			this.indFalNegTB.BackColor = System.Drawing.Color.White;
			this.indFalNegTB.Location = new System.Drawing.Point(962, 401);
			this.indFalNegTB.Name = "indFalNegTB";
			this.indFalNegTB.ReadOnly = true;
			this.indFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.indFalNegTB.TabIndex = 103;
			// 
			// indFalPosTB
			// 
			this.indFalPosTB.BackColor = System.Drawing.Color.White;
			this.indFalPosTB.Location = new System.Drawing.Point(864, 401);
			this.indFalPosTB.Name = "indFalPosTB";
			this.indFalPosTB.ReadOnly = true;
			this.indFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.indFalPosTB.TabIndex = 102;
			// 
			// indTruNegTB
			// 
			this.indTruNegTB.BackColor = System.Drawing.Color.White;
			this.indTruNegTB.Location = new System.Drawing.Point(766, 401);
			this.indTruNegTB.Name = "indTruNegTB";
			this.indTruNegTB.ReadOnly = true;
			this.indTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.indTruNegTB.TabIndex = 101;
			// 
			// indTruPosTB
			// 
			this.indTruPosTB.BackColor = System.Drawing.Color.White;
			this.indTruPosTB.Location = new System.Drawing.Point(668, 401);
			this.indTruPosTB.Name = "indTruPosTB";
			this.indTruPosTB.ReadOnly = true;
			this.indTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.indTruPosTB.TabIndex = 100;
			// 
			// indCorTB
			// 
			this.indCorTB.BackColor = System.Drawing.Color.White;
			this.indCorTB.Location = new System.Drawing.Point(570, 401);
			this.indCorTB.Name = "indCorTB";
			this.indCorTB.ReadOnly = true;
			this.indCorTB.Size = new System.Drawing.Size(82, 20);
			this.indCorTB.TabIndex = 99;
			// 
			// pacFalNegTB
			// 
			this.pacFalNegTB.BackColor = System.Drawing.Color.White;
			this.pacFalNegTB.Location = new System.Drawing.Point(962, 435);
			this.pacFalNegTB.Name = "pacFalNegTB";
			this.pacFalNegTB.ReadOnly = true;
			this.pacFalNegTB.Size = new System.Drawing.Size(83, 20);
			this.pacFalNegTB.TabIndex = 108;
			// 
			// pacFalPosTB
			// 
			this.pacFalPosTB.BackColor = System.Drawing.Color.White;
			this.pacFalPosTB.Location = new System.Drawing.Point(864, 435);
			this.pacFalPosTB.Name = "pacFalPosTB";
			this.pacFalPosTB.ReadOnly = true;
			this.pacFalPosTB.Size = new System.Drawing.Size(83, 20);
			this.pacFalPosTB.TabIndex = 107;
			// 
			// pacTruNegTB
			// 
			this.pacTruNegTB.BackColor = System.Drawing.Color.White;
			this.pacTruNegTB.Location = new System.Drawing.Point(766, 435);
			this.pacTruNegTB.Name = "pacTruNegTB";
			this.pacTruNegTB.ReadOnly = true;
			this.pacTruNegTB.Size = new System.Drawing.Size(83, 20);
			this.pacTruNegTB.TabIndex = 106;
			// 
			// pacTruPosTB
			// 
			this.pacTruPosTB.BackColor = System.Drawing.Color.White;
			this.pacTruPosTB.Location = new System.Drawing.Point(668, 435);
			this.pacTruPosTB.Name = "pacTruPosTB";
			this.pacTruPosTB.ReadOnly = true;
			this.pacTruPosTB.Size = new System.Drawing.Size(82, 20);
			this.pacTruPosTB.TabIndex = 105;
			// 
			// pacCorTB
			// 
			this.pacCorTB.BackColor = System.Drawing.Color.White;
			this.pacCorTB.Location = new System.Drawing.Point(570, 435);
			this.pacCorTB.Name = "pacCorTB";
			this.pacCorTB.ReadOnly = true;
			this.pacCorTB.Size = new System.Drawing.Size(82, 20);
			this.pacCorTB.TabIndex = 104;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1072, 527);
			this.Controls.Add(this.pacFalNegTB);
			this.Controls.Add(this.pacFalPosTB);
			this.Controls.Add(this.pacTruNegTB);
			this.Controls.Add(this.pacTruPosTB);
			this.Controls.Add(this.pacCorTB);
			this.Controls.Add(this.indFalNegTB);
			this.Controls.Add(this.indFalPosTB);
			this.Controls.Add(this.indTruNegTB);
			this.Controls.Add(this.indTruPosTB);
			this.Controls.Add(this.indCorTB);
			this.Controls.Add(this.eurFalNegTB);
			this.Controls.Add(this.eurFalPosTB);
			this.Controls.Add(this.eurTruNegTB);
			this.Controls.Add(this.eurTruPosTB);
			this.Controls.Add(this.eurCorTB);
			this.Controls.Add(this.ausFalNegTB);
			this.Controls.Add(this.ausFalPosTB);
			this.Controls.Add(this.ausTruNegTB);
			this.Controls.Add(this.ausTruPosTB);
			this.Controls.Add(this.ausCorTB);
			this.Controls.Add(this.atlFalNegTB);
			this.Controls.Add(this.atlFalPosTB);
			this.Controls.Add(this.atlTruNegTB);
			this.Controls.Add(this.atlTruPosTB);
			this.Controls.Add(this.atlCorTB);
			this.Controls.Add(this.asiFalNegTB);
			this.Controls.Add(this.asiFalPosTB);
			this.Controls.Add(this.asiTruNegTB);
			this.Controls.Add(this.asiTruPosTB);
			this.Controls.Add(this.asiCorTB);
			this.Controls.Add(this.arcFalNegTB);
			this.Controls.Add(this.arcFalPosTB);
			this.Controls.Add(this.arcTruNegTB);
			this.Controls.Add(this.arcTruPosTB);
			this.Controls.Add(this.arcCorTB);
			this.Controls.Add(this.antFalNegTB);
			this.Controls.Add(this.antFalPosTB);
			this.Controls.Add(this.antTruNegTB);
			this.Controls.Add(this.antTruPosTB);
			this.Controls.Add(this.antCorTB);
			this.Controls.Add(this.ameFalNegTB);
			this.Controls.Add(this.ameFalPosTB);
			this.Controls.Add(this.ameTruNegTB);
			this.Controls.Add(this.ameTruPosTB);
			this.Controls.Add(this.ameCorTB);
			this.Controls.Add(this.afrFalNegTB);
			this.Controls.Add(this.afrFalPosTB);
			this.Controls.Add(this.afrTruNegTB);
			this.Controls.Add(this.afrTruPosTB);
			this.Controls.Add(this.africaCorrectTB);
			this.Controls.Add(this.label22);
			this.Controls.Add(this.label23);
			this.Controls.Add(this.label24);
			this.Controls.Add(this.label25);
			this.Controls.Add(this.label26);
			this.Controls.Add(this.label27);
			this.Controls.Add(this.label28);
			this.Controls.Add(this.label29);
			this.Controls.Add(this.label30);
			this.Controls.Add(this.label31);
			this.Controls.Add(this.label21);
			this.Controls.Add(this.label20);
			this.Controls.Add(this.label19);
			this.Controls.Add(this.label18);
			this.Controls.Add(this.label17);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.runTestBtn);
			this.Controls.Add(this.loadInBinaryObj);
			this.Controls.Add(this.loadTestFileBtn);
			this.Controls.Add(this.showOutputCB);
			this.Controls.Add(this.label16);
			this.Controls.Add(this.label15);
			this.Controls.Add(this.label14);
			this.Controls.Add(this.label13);
			this.Controls.Add(this.label12);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.pacificTB);
			this.Controls.Add(this.indianTB);
			this.Controls.Add(this.europeTB);
			this.Controls.Add(this.australiaTB);
			this.Controls.Add(this.atlanticTB);
			this.Controls.Add(this.asiaTB);
			this.Controls.Add(this.arcticTB);
			this.Controls.Add(this.antarcticaTB);
			this.Controls.Add(this.americaTB);
			this.Controls.Add(this.africaTB);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.outputQueryLabel);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.runTrainingBtn);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.nudEpoch);
			this.Controls.Add(this.nudLearningRate);
			this.Controls.Add(this.outputTxtBox);
			this.Controls.Add(this.loadFileBtn);
			this.Name = "Form1";
			this.Text = "Multi-Layer Neural Network - Backpropagation";
			((System.ComponentModel.ISupportInitialize)(this.nudLearningRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nudEpoch)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button loadFileBtn;
		private System.Windows.Forms.TextBox outputTxtBox;
		private System.Windows.Forms.NumericUpDown nudLearningRate;
		private System.Windows.Forms.NumericUpDown nudEpoch;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button runTrainingBtn;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label outputQueryLabel;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox africaTB;
		private System.Windows.Forms.TextBox americaTB;
		private System.Windows.Forms.TextBox antarcticaTB;
		private System.Windows.Forms.TextBox arcticTB;
		private System.Windows.Forms.TextBox asiaTB;
		private System.Windows.Forms.TextBox atlanticTB;
		private System.Windows.Forms.TextBox australiaTB;
		private System.Windows.Forms.TextBox europeTB;
		private System.Windows.Forms.TextBox indianTB;
		private System.Windows.Forms.TextBox pacificTB;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.CheckBox showOutputCB;
		private System.Windows.Forms.Button loadTestFileBtn;
		private System.Windows.Forms.Button loadInBinaryObj;
		private System.Windows.Forms.Button runTestBtn;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.TextBox africaCorrectTB;
		private System.Windows.Forms.TextBox afrTruPosTB;
		private System.Windows.Forms.TextBox afrTruNegTB;
		private System.Windows.Forms.TextBox afrFalPosTB;
		private System.Windows.Forms.TextBox afrFalNegTB;
		private System.Windows.Forms.TextBox ameFalNegTB;
		private System.Windows.Forms.TextBox ameFalPosTB;
		private System.Windows.Forms.TextBox ameTruNegTB;
		private System.Windows.Forms.TextBox ameTruPosTB;
		private System.Windows.Forms.TextBox ameCorTB;
		private System.Windows.Forms.TextBox antFalNegTB;
		private System.Windows.Forms.TextBox antFalPosTB;
		private System.Windows.Forms.TextBox antTruNegTB;
		private System.Windows.Forms.TextBox antTruPosTB;
		private System.Windows.Forms.TextBox antCorTB;
		private System.Windows.Forms.TextBox arcFalNegTB;
		private System.Windows.Forms.TextBox arcFalPosTB;
		private System.Windows.Forms.TextBox arcTruNegTB;
		private System.Windows.Forms.TextBox arcTruPosTB;
		private System.Windows.Forms.TextBox arcCorTB;
		private System.Windows.Forms.TextBox asiFalNegTB;
		private System.Windows.Forms.TextBox asiFalPosTB;
		private System.Windows.Forms.TextBox asiTruNegTB;
		private System.Windows.Forms.TextBox asiTruPosTB;
		private System.Windows.Forms.TextBox asiCorTB;
		private System.Windows.Forms.TextBox atlFalNegTB;
		private System.Windows.Forms.TextBox atlFalPosTB;
		private System.Windows.Forms.TextBox atlTruNegTB;
		private System.Windows.Forms.TextBox atlTruPosTB;
		private System.Windows.Forms.TextBox atlCorTB;
		private System.Windows.Forms.TextBox ausFalNegTB;
		private System.Windows.Forms.TextBox ausFalPosTB;
		private System.Windows.Forms.TextBox ausTruNegTB;
		private System.Windows.Forms.TextBox ausTruPosTB;
		private System.Windows.Forms.TextBox ausCorTB;
		private System.Windows.Forms.TextBox eurFalNegTB;
		private System.Windows.Forms.TextBox eurFalPosTB;
		private System.Windows.Forms.TextBox eurTruNegTB;
		private System.Windows.Forms.TextBox eurTruPosTB;
		private System.Windows.Forms.TextBox eurCorTB;
		private System.Windows.Forms.TextBox indFalNegTB;
		private System.Windows.Forms.TextBox indFalPosTB;
		private System.Windows.Forms.TextBox indTruNegTB;
		private System.Windows.Forms.TextBox indTruPosTB;
		private System.Windows.Forms.TextBox indCorTB;
		private System.Windows.Forms.TextBox pacFalNegTB;
		private System.Windows.Forms.TextBox pacFalPosTB;
		private System.Windows.Forms.TextBox pacTruNegTB;
		private System.Windows.Forms.TextBox pacTruPosTB;
		private System.Windows.Forms.TextBox pacCorTB;
	}
}

