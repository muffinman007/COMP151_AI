﻿/***
 * All credit goes to the original author: 
 * https://social.technet.microsoft.com/wiki/contents/articles/36428.basis-of-neural-networks-in-c.aspx#Scope
 * Above is the website that I've used to learn backpropagation
 * 
 * This class mimic a neuron cell.
 * The neuron also keep tracks of statistical data:
 *	True Positive
 *	True Negatives
 *	False Positives
 *	False Negatives
 *	
 *	The statistical data is used to test the accuracy of the neural network
 */ 




using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhNeuralNetwork {
	[Serializable]
	class Neuron{

		// inputs' weights
		public List<Dendrite> Dendrites { get; private set; }

		public string Location { get {  return location; } }
		string location;

		public double Bias { get {  return bias; } }
		double bias = 0.0;

		public double Delta { get; private set; }

		public List<double> State { get; private set; }
				
		
		public Neuron(int numberOfInputs, string location) {
			this.bias = TestSimpleRNG.SimpleRNG.GetUniform();
			this.location = location;

			Delta = 0.0;

			// creating the weight
			Dendrites = new List<Dendrite>(numberOfInputs);
			for(int i = 0; i < numberOfInputs; ++i) {
				Dendrites.Add(new Dendrite());
			}
		}


		// used in the training phase of the ANN
		// correction starts in the output layer of the network
		// making adjustment to the output layer
		public void OutputLayerAdjustment(string testLocation) {
			double desiredOutput = (testLocation == location) ? 1.0 : 0.0;
			
			// we can hard code the index of State becuase backpropagation starts in the output layer
			Delta = State[0] * (1 - State[0]) * (desiredOutput - State[0]);
		}


		// used in the training pahse of the ANN
		// making adjustment to the hidden layer of the network
		// weight and delta comes from the associated output layer neuron
		public void HiddenLayerAdjustment(double weight, double delta) {
			Delta = State[0] * (1 - State[0]) * weight * delta;
		}

		// this function is only useful to the input layer
		public void NewStimulus(List<double> inputs) {
			State = inputs;
		}

		// calculate the neuron sigmoid value based on the neighbors' states
		public void Activation(List<Neuron> neighborNeurons) {
			double temp = 0.0;

			int len = neighborNeurons.Count;
			bool isNeighborInputLayer = (neighborNeurons[0].State.Count == 2);

			// the summation of inputs to its' weights
			for(int i = 0; i < len; ++i) {
				List<double> neightborState = neighborNeurons[i].State;
				if(isNeighborInputLayer) {
					temp += (neightborState[0] * Dendrites[i * 2].weight) + (neightborState[1] * Dendrites[i * 2 + 1].weight);
				}
				else {
					// neighbor is the hidden layer
					temp += neightborState[0] * Dendrites[i].weight;
				}
			}

			// update the neuron's internal state
			State = new List<double>(){ Brain.Sigmoid(temp + bias) };
		}
		
		public void UpdateBiasWeight(List<Neuron> neightbor) {
			bias += (Brain.learningRate * Delta);

			// is this the hidden layer weights
			if(neightbor[0].State.Count == 2) { // yes
				for(int i = 0, j = 0; i < Dendrites.Count; i += 2, ++j) {
					Dendrites[i].weight += (Brain.learningRate * neightbor[j].State[0] * Delta);
					Dendrites[i + 1].weight += (Brain.learningRate * neightbor[j].State[1] * Delta);
				}
			}
			else {
				// output layer's weights
				for(int i = 0; i < Dendrites.Count; ++i) {
					Dendrites[i].weight += (Brain.learningRate * neightbor[i].State[0] * Delta);
				}
			}
		}


		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("Neuron Type: " + location);
			sb.AppendLine("Weights:");
			foreach(Dendrite d in Dendrites) {
				sb.AppendLine("\t" + d.weight + Environment.NewLine);
			}
			return sb.ToString();
		}

		#region My single layer implementation
		/*
		// bias always = 1
		// bias has it's own weight
		double bias = 1.0;

		public List<double> Weights { get; set; }

		public string Location { get { return location; } }

		public int State { get { return state; } }

		double learningRate = 0.0;
		string location = string.Empty;
		int state = 0;
		
		public Neuron(string location, double learningRate) : this(location, learningRate, new List<double> {0.0, 0.0}) {}

		public Neuron(string location, double learningRate,  List<double> weights) {
			this.learningRate = learningRate;
			this.location = location;
			Weights = weights;
		}


		public void Learn(List<double> inputs, string testLocation) {

			int desireOutput = (location == testLocation) ? 1 : 0;

			int error = desireOutput - Output(inputs);

			for(int i = 0; i < Weights.Count; ++i) {
				Weights[i] += learningRate * error * inputs[i];
			}
		}

		public int Output(List<double> inputs) {
			double sum = 0.0;
			for(int i = 0; i < Weights.Count; ++i) {
				sum += inputs[i] * Weights[i];
			}	
			
			state = (sum >= 0.0) ? 1 : 0;		
			return state;
		}

		public override string ToString() {
			StringBuilder sb = new StringBuilder();
			sb.AppendLine("Neuron Type: " + location);
			sb.AppendLine("Weights:");
			sb.AppendLine("\t" + Weights[0] + Environment.NewLine + "\t" + Weights[1] + Environment.NewLine + "\t" + Weights[2]);
			return sb.ToString();
		}
		*/
		#endregion
	}
}
