﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhNeuralNetwork {
	class Stats {

		double total = 0.0;
		double truePos = 0.0;
		double trueNeg = 0.0;
		double falsePos = 0.0;
		double falseNeg = 0.0;

		public void AddTruePositive() {
			++truePos;
			++total;
		}

		public void AddTrueNegative() {
			++trueNeg;
			++total;
		}

		public void AddFalsePos() {
			++falsePos;
			++total;
		}

		public void AddFalseNag() {
			++falseNeg;
			++total;
		}

		public double GetCorrect() {
			if(total == 0) return 0.0;

			return (truePos + trueNeg) / total;
		}


		public double GetTruePositive() {
			if(total == 0) return 0.0;
			return truePos/total;
		}

		public double GetTrueNegative() {
			if(total == 0) return 0.0;
			return trueNeg/total;
		}

		public double GetFalsePositive() {
			if(total == 0) return 0.0;
			return falsePos/total;
		}
		
		public double GetFalseNegative() {
			if(total == 0) return 0.0;
			return falseNeg/total;
		}
	}
}
