﻿/***
 * All credit goes to the original author: 
 * https://social.technet.microsoft.com/wiki/contents/articles/36428.basis-of-neural-networks-in-c.aspx#Scope
 * Above is the website that I've used to learn backpropagation
 * 
 * In ANN there's a thing called a layer. Instead of using the word layer, I used the word Lobe.
 * Similary in biology, specific section of the brain is called a lobe. Each lobe contains groups of neuron that
 * does specific thing. In our ANN, each layer or lobe handles one aspect of the computation.  
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhNeuralNetwork {
	[Serializable]
	class Lobe {
		List<Neuron> neuronsList;

		public List<Neuron> Neurons { get { return neuronsList; } }

		public Lobe(int numOfNeurons, List<string> locations, int numberOfInputs) {
			neuronsList = new List<Neuron>(numOfNeurons);
			foreach(var str in locations) {
				neuronsList.Add(new Neuron(numberOfInputs, str));
			}
		}


	}
}
