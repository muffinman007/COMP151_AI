﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinhNeuralNetwork {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
		}

		string brainBinaryFilename = "BrainStructure.binary";

		List<List<double>>	coordinatesList		= new List<List<double>>();
		List<string>		landOceanList		= new List<string>();

		List<string>		uniqueLocationList	= new List<string>() {"Africa", "America", "Antarctica", "Arctic", "Asia", "Atlantic", "Australia", "Europe", "Indian", "Pacific"};

		Dictionary<string, Stats> neuralNetworkStats;

		bool isWeightLoadFromFile = false;

		Brain brain;

		private async void loadFileBtn_Click(object sender, EventArgs e) {
			coordinatesList.Clear();
			landOceanList.Clear();

			bool isLoadSuccess = false;
			if(openFileDialog1.ShowDialog() == DialogResult.OK) {
				await Task.Run(()=> {
					using(StreamReader sr = new StreamReader(openFileDialog1.OpenFile())) {
						double d;
						string str = sr.ReadLine();
						if(str == null) {
							outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.Text = "ERROR: File contain invalid data"; });
							return;
						}

						string[] tokens = str.Split(new char[] {' ', '\t', ',' }, StringSplitOptions.RemoveEmptyEntries);
						if(tokens.Length > 3 || !Double.TryParse(tokens[0], out d)) {
							outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.Text = "ERROR: File contain invalid data"; });
							return;
						}

						// file has correct data format, start loading in the data		
						coordinatesList.Add(new List<double> { Double.Parse(tokens[0]), Double.Parse(tokens[1])});
						landOceanList.Add(tokens[2]);
						
						while((str = sr.ReadLine()) != null) {
							tokens = str.Split(new char[] {' ', '\t', ',' }, StringSplitOptions.RemoveEmptyEntries);
							coordinatesList.Add(new List<double> { Double.Parse(tokens[0]), Double.Parse(tokens[1])});
							landOceanList.Add(tokens[2]);							
						}

						// creating the brain
						brain = new Brain((double)nudLearningRate.Value, uniqueLocationList);
						isLoadSuccess = true;
					}						
				}); // end await task

				if(isLoadSuccess) {
					runTrainingBtn.Enabled = true;
					outputTxtBox.Text = "Training data file loaded successfully." + Environment.NewLine + "Brain created successfully." + Environment.NewLine;
				}
			}
		}


		// when training is done the brain is automatically saved to file
		private async void runTrainingBtn_Click(object sender, EventArgs e) {
			runTrainingBtn.Enabled = false;
			loadFileBtn.Enabled = false;

			nudEpoch.Enabled = false;
			nudLearningRate.Enabled = false;

			Brain.learningRate = (double)nudLearningRate.Value;

			bool isShowOutput = showOutputCB.Checked;

			outputTxtBox.AppendText("Training started..." + Environment.NewLine);
			int epochs = (int)nudEpoch.Value;
			await Task.Run(()=> {
				while(epochs > 0) {
					for(int i = 0; i < coordinatesList.Count; ++i) {
						if(isShowOutput) {
							List<Neuron> outputNeurons = brain.LearnAndShow(coordinatesList[i], landOceanList[i]);
						
							foreach(Neuron n in outputNeurons) {
								switch(n.Location) {
									case "Africa":
										africaTB.InvokeIfRequired(()=> { africaTB.Text = n.State[0].ToString(); });
										break;
									case "America":
										americaTB.InvokeIfRequired(()=> { americaTB.Text = n.State[0].ToString(); });
										break;
									case "Antarctica":
										antarcticaTB.InvokeIfRequired(()=> { antarcticaTB.Text = n.State[0].ToString(); });
										break;
									case "Arctic":
										arcticTB.InvokeIfRequired(()=> { arcticTB.Text = n.State[0].ToString(); });
										break;
									case "Asia":
										asiaTB.InvokeIfRequired(()=> { asiaTB.Text = n.State[0].ToString(); });
										break;
									case "Atlantic":
										atlanticTB.InvokeIfRequired(()=> { atlanticTB.Text = n.State[0].ToString(); });
										break;
									case "Australia":
										australiaTB.InvokeIfRequired(()=> { australiaTB.Text = n.State[0].ToString(); });
										break;
									case "Europe":
										europeTB.InvokeIfRequired(()=> { europeTB.Text = n.State[0].ToString(); });
										break;
									case "Indian":
										indianTB.InvokeIfRequired(()=> { indianTB.Text = n.State[0].ToString(); });
										break;
									case "Pacific":
										pacificTB.InvokeIfRequired(()=> { pacificTB.Text = n.State[0].ToString(); });
										break;
								}
							}
						}// 
						else {
							brain.Learn(coordinatesList[i], landOceanList[i]);
						}
					}// end for loop
					--epochs;
				}
			});

			outputTxtBox.AppendText("Training finish successfully." + Environment.NewLine);
			outputTxtBox.AppendText(Environment.NewLine + "Output layer neurons and weights:" + Environment.NewLine);
			foreach(Neuron n in brain.OutputLayerNeurons()) {
				outputTxtBox.AppendText(n.ToString() + Environment.NewLine);
			}

			outputTxtBox.AppendText(Environment.NewLine + "Saving brain structure to file." + Environment.NewLine);
			// save the brain binary structure to file
			await Task.Run(()=> {
				BinaryFormatter bf = new BinaryFormatter();
				FileStream fsout = new FileStream(brainBinaryFilename, FileMode.Create, FileAccess.Write, FileShare.None);
				try {
					using(fsout) {
						bf.Serialize(fsout, brain);
					}
					outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Save successfully." + Environment.NewLine + "Brain data saved as " + brainBinaryFilename + Environment.NewLine); });
				}
				catch {
					outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("ERROR Saving brain structure to file." + Environment.NewLine); });
				}
			});

			// finish training can load in the test file for testing
			loadTestFileBtn.Enabled = true;
			runTrainingBtn.Enabled = true;
			loadFileBtn.Enabled = true;
		}

		private async void loadInBinaryObj_Click(object sender, EventArgs e) {
			outputTxtBox.AppendText("Loading in brain data." + Environment.NewLine);
			bool isLoadedIn = false;
			if(openFileDialog1.ShowDialog() == DialogResult.OK) {
				await Task.Run(()=> {
					BinaryFormatter bf = new BinaryFormatter();
					FileStream fsin = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read, FileShare.None);
					try {
						using(fsin) {
							brain = (Brain)bf.Deserialize(fsin);
						}
						outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Brain loaded successfull." + Environment.NewLine); });
						isLoadedIn = true;
					}
					catch {
						outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("ERROR loading in brain data." + Environment.NewLine); });
					}
				});
			}

			if(isLoadedIn) loadTestFileBtn.Enabled = true;
		}

		private async void loadTestFileBtn_Click(object sender, EventArgs e) {
			coordinatesList.Clear();
			landOceanList.Clear();

			outputTxtBox.AppendText("Loading in test data." + Environment.NewLine);
			if(openFileDialog1.ShowDialog() == DialogResult.OK) {
				await Task.Run(()=> {
					using(StreamReader sr = new StreamReader(openFileDialog1.OpenFile())) {
						double d;
						string str = sr.ReadLine();
						if(str == null) {
							outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.Text = "ERROR: File contain invalid data"; });
							return;
						}

						string[] tokens = str.Split(new char[] {' ', '\t', ',' }, StringSplitOptions.RemoveEmptyEntries);
						if(tokens.Length > 3 || !Double.TryParse(tokens[0], out d)) {
							outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.Text = "ERROR: File contain invalid data"; });
							return;
						}

						// file has correct data format, start loading in the data		
						coordinatesList.Add(new List<double> { Double.Parse(tokens[0]), Double.Parse(tokens[1])});
						landOceanList.Add(tokens[2]);
						
						while((str = sr.ReadLine()) != null) {
							tokens = str.Split(new char[] {' ', '\t', ',' }, StringSplitOptions.RemoveEmptyEntries);
							coordinatesList.Add(new List<double> { Double.Parse(tokens[0]), Double.Parse(tokens[1])});
							landOceanList.Add(tokens[2]);							
						}
					}
				});
			}

			outputTxtBox.AppendText("Test data load successfully." + Environment.NewLine);
			runTestBtn.Enabled = true;
		}

		private async void runTestBtn_Click(object sender, EventArgs e) {
			bool stateOfLoadTrainBtn = loadFileBtn.Enabled;
			bool stateOfRunTrainBtn = runTrainingBtn.Enabled;
			loadFileBtn.Enabled = false;
			runTrainingBtn.Enabled = false;

			neuralNetworkStats = new Dictionary<string, Stats>();
			foreach(string s in uniqueLocationList) {
				neuralNetworkStats.Add(s, new Stats());
			}

			
			outputTxtBox.AppendText("Running test..." + Environment.NewLine);

			await Task.Run(()=> {		
				for(int i = 0; i < coordinatesList.Count; ++i) {
					List<Neuron> outputNeuron = brain.ClassifyData(coordinatesList[i]);
									

					// find the most excited neuron(s)
					List<Neuron> mostExcitedNeuron = new List<Neuron>() { outputNeuron[0] };
					for(int j = 1; j < outputNeuron.Count; ++j) {
						if(outputNeuron[j].State[0] > mostExcitedNeuron[0].State[0]) {
							mostExcitedNeuron.Clear();
							mostExcitedNeuron.Add(outputNeuron[j]);
						}
						else if(outputNeuron[j].State[0] == mostExcitedNeuron[0].State[0]) {
							mostExcitedNeuron.Add(outputNeuron[j]);
						}
					}


					// query data
					// if the network works 100 percent correctly, there should only be one excited neuron
					foreach(Neuron n in outputNeuron) {
						if(mostExcitedNeuron.Contains(n)) {
							if(n.Location == landOceanList[i]) {
								//outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText(n.State[0].ToString() + Environment.NewLine); });
								neuralNetworkStats[n.Location].AddTruePositive();
							}
							else {
								neuralNetworkStats[n.Location].AddFalsePos();
							}
						}
						else {
							if(n.Location == landOceanList[i]) {
								neuralNetworkStats[n.Location].AddFalseNag();
							}
							else {
								neuralNetworkStats[n.Location].AddTrueNegative();
							}
						}
					}

					// update UI with information					
					foreach(string k in neuralNetworkStats.Keys.ToList()) {
						switch(k) {
							case "Africa":
								africaCorrectTB.InvokeIfRequired(()=> { africaCorrectTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								afrTruPosTB.InvokeIfRequired(()=> { afrTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								afrTruNegTB.InvokeIfRequired(()=> { afrTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								afrFalPosTB.InvokeIfRequired(()=> { afrFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								afrFalNegTB.InvokeIfRequired(()=> { afrFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "America":
								ameCorTB.InvokeIfRequired(()=>    { ameCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								ameTruPosTB.InvokeIfRequired(()=> { ameTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								ameTruNegTB.InvokeIfRequired(()=> { ameTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								ameFalPosTB.InvokeIfRequired(()=> { ameFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								ameFalNegTB.InvokeIfRequired(()=> { ameFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "Antarctica":
								antCorTB.InvokeIfRequired(()=>    { antCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								antTruPosTB.InvokeIfRequired(()=> { antTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								antTruNegTB.InvokeIfRequired(()=> { antTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								antFalPosTB.InvokeIfRequired(()=> { antFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								antFalNegTB.InvokeIfRequired(()=> { antFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "Arctic":
								arcCorTB.InvokeIfRequired(()=>    { arcCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								arcTruPosTB.InvokeIfRequired(()=> { arcTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								arcTruNegTB.InvokeIfRequired(()=> { arcTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								arcFalPosTB.InvokeIfRequired(()=> { arcFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								arcFalNegTB.InvokeIfRequired(()=> { arcFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "Asia":
								asiCorTB.InvokeIfRequired(()=>    { asiCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								asiTruPosTB.InvokeIfRequired(()=> { asiTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								asiTruNegTB.InvokeIfRequired(()=> { asiTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								asiFalPosTB.InvokeIfRequired(()=> { asiFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								asiFalNegTB.InvokeIfRequired(()=> { asiFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "Atlantic":
								atlCorTB.InvokeIfRequired(()=>    { atlCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								atlTruPosTB.InvokeIfRequired(()=> { atlTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								atlTruNegTB.InvokeIfRequired(()=> { atlTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								atlFalPosTB.InvokeIfRequired(()=> { atlFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								atlFalNegTB.InvokeIfRequired(()=> { atlFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "Australia":
								ausCorTB.InvokeIfRequired(()=>    { ausCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								ausTruPosTB.InvokeIfRequired(()=> { ausTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								ausTruNegTB.InvokeIfRequired(()=> { ausTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								ausFalPosTB.InvokeIfRequired(()=> { ausFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								ausFalNegTB.InvokeIfRequired(()=> { ausFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "Europe":
								eurCorTB.InvokeIfRequired(()=>    { eurCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								eurTruPosTB.InvokeIfRequired(()=> { eurTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								eurTruNegTB.InvokeIfRequired(()=> { eurTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								eurFalPosTB.InvokeIfRequired(()=> { eurFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								eurFalNegTB.InvokeIfRequired(()=> { eurFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "Indian":
								indCorTB.InvokeIfRequired(()=>    { indCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								indTruPosTB.InvokeIfRequired(()=> { indTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								indTruNegTB.InvokeIfRequired(()=> { indTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								indFalPosTB.InvokeIfRequired(()=> { indFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								indFalNegTB.InvokeIfRequired(()=> { indFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
							case "Pacific":
								pacCorTB.InvokeIfRequired(()=>    { pacCorTB.Text = neuralNetworkStats[k].GetCorrect().ToString(); });
								pacTruPosTB.InvokeIfRequired(()=> { pacTruPosTB.Text = neuralNetworkStats[k].GetTruePositive().ToString(); });
								pacTruNegTB.InvokeIfRequired(()=> { pacTruNegTB.Text = neuralNetworkStats[k].GetTrueNegative().ToString(); });
								pacFalPosTB.InvokeIfRequired(()=> { pacFalPosTB.Text = neuralNetworkStats[k].GetFalsePositive().ToString(); });
								pacFalNegTB.InvokeIfRequired(()=> { pacFalNegTB.Text = neuralNetworkStats[k].GetFalseNegative().ToString(); });
								break;
						}
					}
				}
			});
		
			outputTxtBox.AppendText("Test completed successfully." + Environment.NewLine);

			loadFileBtn.Enabled = stateOfLoadTrainBtn;
			runTrainingBtn.Enabled = stateOfRunTrainBtn;
		}
	}// end forms


	public static class ControlExtension {
		public static void InvokeIfRequired(this Control control, Action action) {
			if(control.InvokeRequired) {
				control.Invoke(action);
			}
			else {
				action();
			}
		}
	}

}
