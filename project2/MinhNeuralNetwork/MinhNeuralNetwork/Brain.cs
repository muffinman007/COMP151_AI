﻿/***
 * All credit goes to the original author: 
 * https://social.technet.microsoft.com/wiki/contents/articles/36428.basis-of-neural-networks-in-c.aspx#Scope
 * Above is the website that I've used to learn backpropagation
 * 
 * The backpropagation ANN is represented by the Brain. The brain will contains a number of lobe, each lobe contains a number of neuron, and each 
 * neuron contains a number of dendrites.   
 * 
 * This artifical neural network is generalized to the project requirments. It will not work
 * for any other types of classificaion problem. 
 * 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhNeuralNetwork {
	[Serializable]
	class Brain {
		public static double learningRate = 0.0;

		List<Lobe> lobesList = new List<Lobe>();

		public Brain(double learningRate, List<String> locations) {
			Brain.learningRate = learningRate;

			// creating each lobe
			for(int i = 0; i < 3; ++i) {
				switch(i) {
					case 0: // the input layer
						lobesList.Add(new Lobe(10, locations, 2));
						break;
					case 1: // the hidden layer
						lobesList.Add(new Lobe(10, locations, locations.Count * 2));
						break;
					case 2: // the output layer
						lobesList.Add(new Lobe(10, locations, locations.Count));
						break;
				}				
			}
		}


		// use gradient descent, which is differentable. 
		// use sigmoid function to introduce nonlinearity in the model.
		// The sigmoid function maps arbitrary real values back to the range [0, 1]. The larger the value, the closer to 1 you’ll get. - https://nathanbrixius.wordpress.com/2016/06/04/functions-i-have-known-logit-and-sigmoid/
		static public double Sigmoid(double x) {
			return 1.0 / (1.0 + Math.Exp(-x));
		}


		public List<Neuron> LearnAndShow(List<double> inputs, string testLocation) {
			ClassifyData(inputs);

			// backpropagation
			// the output layer
			List<Neuron> outputLayerNeurons = lobesList[lobesList.Count - 1].Neurons;
			// the hidden layer
			List<Neuron> hiddenLayerNeurons = lobesList[lobesList.Count - 2].Neurons;

			foreach(Neuron outputNeuron in outputLayerNeurons) {
				// updating the delta value
				outputNeuron.OutputLayerAdjustment(testLocation);
				
				// now go through each of the neurons in the hidden layer
				// and update the delta based on the current updated output neuron states
				for(int i = 0; i < hiddenLayerNeurons.Count; ++i) {
					hiddenLayerNeurons[i].HiddenLayerAdjustment(outputNeuron.Dendrites[i].weight, outputNeuron.Delta);
				}				
			}
			
			// update the bias and weights for each neurons
			// start with the output layer
			foreach(Neuron n in outputLayerNeurons) {
				n.UpdateBiasWeight(hiddenLayerNeurons);
			}
			// next is the hidden layer
			foreach(Neuron n in hiddenLayerNeurons) {
				n.UpdateBiasWeight(lobesList[0].Neurons); // feed the input layer to the function
			}

			// return a query data
			return outputLayerNeurons;
		}

		public void Learn(List<double> inputs, string testLocation) {
			ClassifyData(inputs);

			// backpropagation
			// the output layer
			List<Neuron> outputLayerNeurons = lobesList[lobesList.Count - 1].Neurons;
			// the hidden layer
			List<Neuron> hiddenLayerNeurons = lobesList[lobesList.Count - 2].Neurons;

			foreach(Neuron outputNeuron in outputLayerNeurons) {
				// updating the delta value
				outputNeuron.OutputLayerAdjustment(testLocation);
				
				// now go through each of the neurons in the hidden layer
				// and update the delta based on the current updated output neuron states
				for(int i = 0; i < hiddenLayerNeurons.Count; ++i) {
					hiddenLayerNeurons[i].HiddenLayerAdjustment(outputNeuron.Dendrites[i].weight, outputNeuron.Delta);
				}				
			}
			
			// update the bias and weights for each neurons
			// start with the output layer
			foreach(Neuron n in outputLayerNeurons) {
				n.UpdateBiasWeight(hiddenLayerNeurons);
			}
			// next is the hidden layer
			foreach(Neuron n in hiddenLayerNeurons) {
				n.UpdateBiasWeight(lobesList[0].Neurons); // feed the input layer to the function
			}
		}


		// forward feeding
		public List<Neuron> ClassifyData(List<double> inputs) {
			for(int i = 0; i < lobesList.Count; ++i) {
				List<Neuron> neurons = lobesList[i].Neurons;
				if(i == 0) {					
					// the input layer
					foreach(Neuron n in neurons) {
						// feed the input data to the network
						n.NewStimulus(inputs);
					}
				}
				else {
					// calculate each neuron sigmoid value
					foreach(Neuron n in neurons) {
						n.Activation(lobesList[i - 1].Neurons);
					}
				}
			}

			// return the ANN result			
			return lobesList[lobesList.Count - 1].Neurons;
		}


		public List<Neuron> OutputLayerNeurons() {
			return lobesList[lobesList.Count - 1].Neurons;
		}

	}
}
