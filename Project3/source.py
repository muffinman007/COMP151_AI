from bs4 import BeautifulSoup as soup
from urllib.request import urlopen as uReq


movie_names = ['mad_max_fury_road', 'inside_out_2015', 'moonlight_2016', 'get_out', 'justice_league_2017', 'wonder', 'thor_ragnarok_2017', 'daddys_home_2', 'last_airbender', 'pirates_of_the_caribbean_dead_men_tell_no_tales', 'baywatch_2017', 'the_mummy_2017', 'the_dark_tower_2017', 'transformers_the_last_knight_2017', 'the_emoji_movie', 'quest_2017', 'the_white_king', 'i_tonya', 'murder_on_the_orient_express_2017', 'three_billboards_outside_ebbing_missouri', 'lady_bird', 'the_star_2017', 'ballistic_ecks_vs_sever', 'jawbreaker', 'chill_factor', 'battlefield_earth', 'corky_romano', 'because_i_said_so', 'alone_in_the_dark', 'basic_instinct_2', 'twisted', 'gigli', 'i_still_know_what_you_did_last_summer', 'night_at_the_roxbury', '1094646-eye_of_the_beholder', 'end_of_days', 'mod_squad']


prefix = 'https://www.rottentomatoes.com/m/'
parts = '/reviews/?page='
end = '&sort='
#https://www.rottentomatoes.com/m/wonder/reviews/?page=2&sort=

for movie in movie_names:
    for i in range(1, 25):
        url = prefix + movie + parts + str(i) + end
        print(url)
        
        try:
            uClient = uReq(url)
    
            page_html = uClient.read()
    
            uClient.close()
    
            page_soup = soup(page_html, "html.parser")
    
    
            a = page_soup.find_all(class_='review_icon')
            b = page_soup.find_all(class_ = 'the_review')
            d = {}
    
            for i in range(len(b)):
                d[b[i].string] = a[i]
    
            bad_review = open('bad.txt', 'a+')
            good_review = open('good.txt', 'a+')
    		
            try:
                for k, v in d.items():
                    if 'fresh' in str(v):
                        good_review.write(k)
                    elif 'rotten' in str(v):
                        bad_review.write(k)
                    else:
                        raise 'wrong value!'
            except:
                print("Exception: write to file. Continue.")
    
            bad_review.close()
            good_review.close()
        except:
            print("URL error, continue with next iteration.")

