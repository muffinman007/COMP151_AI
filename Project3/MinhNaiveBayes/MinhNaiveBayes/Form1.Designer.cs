﻿namespace MinhNaiveBayes {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.loadFreshBtn = new System.Windows.Forms.Button();
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.loadRottenBtn = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.outputTxtBox = new System.Windows.Forms.TextBox();
			this.predictReviewBtn = new System.Windows.Forms.Button();
			this.loadTestFileBtn = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.loadBinaryBtn = new System.Windows.Forms.Button();
			this.resetBtn = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// loadFreshBtn
			// 
			this.loadFreshBtn.Location = new System.Drawing.Point(12, 35);
			this.loadFreshBtn.Name = "loadFreshBtn";
			this.loadFreshBtn.Size = new System.Drawing.Size(133, 23);
			this.loadFreshBtn.TabIndex = 0;
			this.loadFreshBtn.Text = "Load Fresh Review";
			this.loadFreshBtn.UseVisualStyleBackColor = true;
			this.loadFreshBtn.Click += new System.EventHandler(this.loadFreshBtn_Click);
			// 
			// loadRottenBtn
			// 
			this.loadRottenBtn.Location = new System.Drawing.Point(151, 35);
			this.loadRottenBtn.Name = "loadRottenBtn";
			this.loadRottenBtn.Size = new System.Drawing.Size(130, 23);
			this.loadRottenBtn.TabIndex = 1;
			this.loadRottenBtn.Text = "Load Rotten Review";
			this.loadRottenBtn.UseVisualStyleBackColor = true;
			this.loadRottenBtn.Click += new System.EventHandler(this.loadRottenBtn_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(13, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(69, 16);
			this.label1.TabIndex = 2;
			this.label1.Text = "Training:";
			// 
			// outputTxtBox
			// 
			this.outputTxtBox.BackColor = System.Drawing.Color.White;
			this.outputTxtBox.Location = new System.Drawing.Point(287, 12);
			this.outputTxtBox.MaxLength = 100000;
			this.outputTxtBox.Multiline = true;
			this.outputTxtBox.Name = "outputTxtBox";
			this.outputTxtBox.ReadOnly = true;
			this.outputTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.outputTxtBox.Size = new System.Drawing.Size(359, 280);
			this.outputTxtBox.TabIndex = 3;
			// 
			// predictReviewBtn
			// 
			this.predictReviewBtn.Enabled = false;
			this.predictReviewBtn.Location = new System.Drawing.Point(153, 159);
			this.predictReviewBtn.Name = "predictReviewBtn";
			this.predictReviewBtn.Size = new System.Drawing.Size(128, 23);
			this.predictReviewBtn.TabIndex = 4;
			this.predictReviewBtn.Text = "Predict Test Review";
			this.predictReviewBtn.UseVisualStyleBackColor = true;
			this.predictReviewBtn.Click += new System.EventHandler(this.predictReviewBtn_Click);
			// 
			// loadTestFileBtn
			// 
			this.loadTestFileBtn.Enabled = false;
			this.loadTestFileBtn.Location = new System.Drawing.Point(12, 159);
			this.loadTestFileBtn.Name = "loadTestFileBtn";
			this.loadTestFileBtn.Size = new System.Drawing.Size(133, 23);
			this.loadTestFileBtn.TabIndex = 5;
			this.loadTestFileBtn.Text = "Load Test File";
			this.loadTestFileBtn.UseVisualStyleBackColor = true;
			this.loadTestFileBtn.Click += new System.EventHandler(this.loadTestFileBtn_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(13, 128);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(179, 16);
			this.label2.TabIndex = 6;
			this.label2.Text = "Classification Prediction:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(13, 214);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(194, 16);
			this.label3.TabIndex = 7;
			this.label3.Text = "Load Classifier Binary File:";
			// 
			// loadBinaryBtn
			// 
			this.loadBinaryBtn.Location = new System.Drawing.Point(12, 244);
			this.loadBinaryBtn.Name = "loadBinaryBtn";
			this.loadBinaryBtn.Size = new System.Drawing.Size(133, 23);
			this.loadBinaryBtn.TabIndex = 8;
			this.loadBinaryBtn.Text = "Load Binary File";
			this.loadBinaryBtn.UseVisualStyleBackColor = true;
			this.loadBinaryBtn.Click += new System.EventHandler(this.loadBinaryBtn_Click);
			// 
			// resetBtn
			// 
			this.resetBtn.Location = new System.Drawing.Point(12, 73);
			this.resetBtn.Name = "resetBtn";
			this.resetBtn.Size = new System.Drawing.Size(133, 23);
			this.resetBtn.TabIndex = 9;
			this.resetBtn.Text = "Reset System";
			this.resetBtn.UseVisualStyleBackColor = true;
			this.resetBtn.Click += new System.EventHandler(this.resetBtn_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(663, 304);
			this.Controls.Add(this.resetBtn);
			this.Controls.Add(this.loadBinaryBtn);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.loadTestFileBtn);
			this.Controls.Add(this.predictReviewBtn);
			this.Controls.Add(this.outputTxtBox);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.loadRottenBtn);
			this.Controls.Add(this.loadFreshBtn);
			this.Name = "Form1";
			this.Text = "Naive Bayes Rotten Tomatoes Classifier";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button loadFreshBtn;
		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button loadRottenBtn;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox outputTxtBox;
		private System.Windows.Forms.Button predictReviewBtn;
		private System.Windows.Forms.Button loadTestFileBtn;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button loadBinaryBtn;
		private System.Windows.Forms.Button resetBtn;
	}
}

