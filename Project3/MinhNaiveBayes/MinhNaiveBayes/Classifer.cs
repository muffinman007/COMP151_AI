﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhNaiveBayes {
	[Serializable]
	class Classifer {

		public Dictionary<string,decimal> FreshProbability { get; private set; }		
		public Dictionary<string, int> FreshUniqueWordCount { get; private set; }

		public Dictionary<string, decimal> RottenProbability { get; private set; }
		public Dictionary<string, int> RottenUniqueWordCount { get; private set; }

		public int TotalFreshWordCount { get; private set; }
		public int TotalRottenWordCount { get; private set; }
	


		//List<string> commonWords = new List<string>() {
		//	"the", "a", "that", "i", "it", "as", "this", "they", "he", "she", "they", "an", "would", "there", "their", "to", "of", "its", "in", "title"
		//};

		public Classifer(string freshData, string rottenData) {
			FreshProbability = new Dictionary<string, decimal>();
			FreshUniqueWordCount = new Dictionary<string, int>();
			RottenProbability = new Dictionary<string, decimal>();
			RottenUniqueWordCount = new Dictionary<string, int>();
			
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Fresh review
			List<string> tokens = new List<string>(freshData.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries));
			
			TotalFreshWordCount = tokens.Count;

			// counting the words in the data 
			foreach(string str in tokens) {
				// skip commonWords
				//if(commonWords.Contains(str)) continue;
				if(str == "-") continue;

				if(FreshUniqueWordCount.ContainsKey(str)) {
					++FreshUniqueWordCount[str];		// increment the count
				}
				else {
					FreshUniqueWordCount.Add(str, 1);	// the first word in the list
				}
			}
			
			int numUniqueWord = FreshUniqueWordCount.Count;

			foreach(var pair in FreshUniqueWordCount) {
				// using Laplace/Laplacian smoothing
				FreshProbability.Add(pair.Key, (pair.Value + 1.0m)/(TotalFreshWordCount + numUniqueWord));
			}

			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			// Rotten review
			tokens.Clear();
			tokens = new List<string>(rottenData.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries));

			TotalRottenWordCount = tokens.Count;

			// counting the words in the data 
			foreach(string str in tokens) {
				// skip commonWords
				//if(commonWords.Contains(str)) continue;
				if(str == "-") continue;

				if(RottenUniqueWordCount.ContainsKey(str)) {
					++RottenUniqueWordCount[str];		// increment the count
				}
				else {
					RottenUniqueWordCount.Add(str, 1);	// the first word in the list
				}
			}
			
			numUniqueWord = RottenUniqueWordCount.Count;

			foreach(var pair in RottenUniqueWordCount) {
				// using Laplace/Laplacian smoothing
				RottenProbability.Add(pair.Key, (pair.Value + 1.0m)/(TotalRottenWordCount + numUniqueWord));
			}
		}// ctor



		public decimal FreshPrediction(string testString) {
			decimal A = Prediction(testString, (1m/(TotalFreshWordCount + FreshUniqueWordCount.Count)), FreshProbability);
			decimal B = Prediction(testString, (1m/(TotalRottenWordCount + RottenUniqueWordCount.Count)), RottenProbability);

			return A / (A + B);
		}

		public decimal RottenPrediction(string testString) {
			decimal A = Prediction(testString, (1m/(TotalFreshWordCount + FreshUniqueWordCount.Count)), FreshProbability);
			decimal B = Prediction(testString, (1m/(TotalRottenWordCount + RottenUniqueWordCount.Count)), RottenProbability);

			return B / (A + B);
		}

		decimal Prediction(string testString, decimal LaplacianSmoothing, Dictionary<string,decimal> probability) {
			List<string> tokens = new List<string>(testString.Split(new char[] {' ' }, StringSplitOptions.RemoveEmptyEntries));

			decimal p = 1m;

			foreach(string s in tokens) {
				if(probability.ContainsKey(s)) {
					p *= probability[s] * 1000;
				}
				else {
					// laplace/laplacian smoothing
					p *= LaplacianSmoothing * 1000;
				}
			}

			p *= 0.5m * 1000;		// there's only two type of data set, fresh and rotten . so one or the other is 50/50

			return p;
		}

	}
}
