﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MinhNaiveBayes {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
			outputTxtBox.AppendText("Load in fresh and rotten review." + Environment.NewLine);
		}

		string freshTrainingData = String.Empty;
		string rottenTrainingData = String.Empty;
		string testData = String.Empty;
		Classifer classifer = null;

		readonly string BinaryFilename = "NaiveBayesClassifier.binary";
		readonly string FreshDumpFilename = "NaiveBayesFreshDump.csv";
		readonly string RottenDumpFilename = "NaiveBayesRottenDump.csv";


		async Task InstantiateClassifer() {
			await Task.Run(()=> {
				classifer = new Classifer(freshTrainingData, rottenTrainingData);

				outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Initializing classifer successfully." + Environment.NewLine); });

				// save the binary to file
				BinaryFormatter bf = new BinaryFormatter();
				FileStream fsout = new FileStream(BinaryFilename, FileMode.Create, FileAccess.Write, FileShare.None);
				try {
					using(fsout) {
						bf.Serialize(fsout, classifer);
					}
					outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Save classifier binary successfully." + Environment.NewLine + "Classifier saved as " + BinaryFilename + Environment.NewLine); });
				}
				catch {
					outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("ERROR Saving classifier binary to file." + Environment.NewLine); });
				}

				outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Data dump to file..." + Environment.NewLine); });

				// data dump
				StringBuilder sb = new StringBuilder();
				sb.AppendLine("Fresh data");
				sb.AppendLine("Fresh Total Word Count," + classifer.TotalFreshWordCount);
				sb.AppendLine("Word,Probability");
				foreach(var pair in classifer.FreshProbability) {
					sb.AppendLine(pair.Key + "," + pair.Value);
				}
				using(StreamWriter sw = new StreamWriter(FreshDumpFilename)) {
					sw.Write(sb.ToString());
				}

				sb.Clear();
				sb.AppendLine("Rotten data");
				sb.AppendLine("Rotten Total Word Count," + classifer.TotalRottenWordCount);
				sb.AppendLine("Word,Probability");
				foreach(var pair in classifer.RottenProbability) {
					sb.AppendLine(pair.Key + "," + pair.Value);
				}
				using(StreamWriter sw = new StreamWriter(RottenDumpFilename)) {
					sw.Write(sb.ToString());
				}

				outputTxtBox.InvokeIfRequired(()=> {
					outputTxtBox.AppendText("Data dump finished." + Environment.NewLine +
											"Dump filename:" + Environment.NewLine +
											FreshDumpFilename + Environment.NewLine +
											RottenDumpFilename + Environment.NewLine);
				});
			});
		}

		private async void loadFreshBtn_Click(object sender, EventArgs e) {
			bool isLoadSuccess = false;
			outputTxtBox.AppendText("Loading in fresh review data..." + Environment.NewLine);
			if(openFileDialog1.ShowDialog() == DialogResult.OK) {
				await Task.Run(()=> {
					using(StreamReader sr = new StreamReader(openFileDialog1.OpenFile())) {
						freshTrainingData = sr.ReadToEnd().ToLower();
					}
					isLoadSuccess = true;
				});
			}

			if(isLoadSuccess) {
				outputTxtBox.AppendText("Fresh review data load successfully." + Environment.NewLine);

				if(rottenTrainingData != String.Empty) {
					outputTxtBox.AppendText("Initializing classifer..." + Environment.NewLine);
					await InstantiateClassifer();
					loadTestFileBtn.Enabled = true;
				}
			}
			else {
				outputTxtBox.AppendText("Cancel operation." + Environment.NewLine);
			}

		}

		private async void loadRottenBtn_Click(object sender, EventArgs e) {
			bool isLoadSuccess = false;
			outputTxtBox.AppendText("Loading in rotten review data..." + Environment.NewLine);
			if(openFileDialog1.ShowDialog() == DialogResult.OK) {
				await Task.Run(()=> {				
					using(StreamReader sr = new StreamReader(openFileDialog1.OpenFile())) {
						rottenTrainingData = sr.ReadToEnd().ToLower();
					}
					isLoadSuccess = true;
				});
			}

			if(isLoadSuccess) {
				outputTxtBox.AppendText("Rotten review data load successfully." + Environment.NewLine);

				if(freshTrainingData != String.Empty) {
					outputTxtBox.AppendText("Initializing classifer..." + Environment.NewLine);
					await InstantiateClassifer();
					loadTestFileBtn.Enabled = true;
				}
			}
			else {
				outputTxtBox.AppendText("Cancel operation." + Environment.NewLine);
			}
		}

		private async void loadBinaryBtn_Click(object sender, EventArgs e) {
			outputTxtBox.AppendText("Loading in classifer binary." + Environment.NewLine);
			bool isLoadedIn = false;	// check for loading error.
			if(openFileDialog1.ShowDialog() == DialogResult.OK) {
				await Task.Run(()=> {
					BinaryFormatter bf = new BinaryFormatter();
					FileStream fsin = new FileStream(openFileDialog1.FileName, FileMode.Open, FileAccess.Read, FileShare.None);
					try {
						using(fsin) {
							classifer = (Classifer)bf.Deserialize(fsin);
						}
						outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Classifer binary loaded successfull." + Environment.NewLine); });
						isLoadedIn = true;
					}
					catch {
						outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("ERROR loading in classifer binary." + Environment.NewLine); });
					}
				});
			}

			if(isLoadedIn) {
				loadTestFileBtn.Enabled = true;
			}
			else {
				outputTxtBox.AppendText("Cancel operation." + Environment.NewLine);
			}
		}


		private async void loadTestFileBtn_Click(object sender, EventArgs e) {
			bool isLoadSuccess = false;
			outputTxtBox.AppendText("Loading in test review data..." + Environment.NewLine);
			if(openFileDialog1.ShowDialog() == DialogResult.OK) {
				await Task.Run(()=> {				
					using(StreamReader sr = new StreamReader(openFileDialog1.OpenFile())) {
						testData = sr.ReadToEnd().ToLower();
					}		
					isLoadSuccess = true;		
				});
			}

			if(isLoadSuccess) {
				outputTxtBox.AppendText("Test data load successfully." + Environment.NewLine);
				predictReviewBtn.Enabled = true;
			}
			else {
				outputTxtBox.AppendText("Cancel operation." + Environment.NewLine);
			}		
		}

		private void predictReviewBtn_Click(object sender, EventArgs e) {
			outputTxtBox.AppendText(Environment.NewLine + Environment.NewLine + "Running prediction test..." + Environment.NewLine);

			decimal freshProb = classifer.FreshPrediction(testData);
			decimal rottenProb = classifer.RottenPrediction(testData);

			outputTxtBox.AppendText("Prediction result:" + Environment.NewLine + 
									"Fresh Probability: " + Environment.NewLine + 
									"\t" + freshProb + Environment.NewLine +
									"Rotten Prbability: " + Environment.NewLine +
									"\t" + rottenProb + Environment.NewLine);

			if(freshProb > rottenProb) {
				outputTxtBox.AppendText("Verdict:" + Environment.NewLine + "The review is fresh." + Environment.NewLine + Environment.NewLine);
			}
			else if(rottenProb > freshProb) {
				outputTxtBox.AppendText("Verdict:" + Environment.NewLine + "The review is rotten." + Environment.NewLine + Environment.NewLine);
			}
			else {
				outputTxtBox.AppendText("Verdict:" + Environment.NewLine + "The review neither fresh nor rotten." + Environment.NewLine + Environment.NewLine);
			}
		}


		private void resetBtn_Click(object sender, EventArgs e) {
			freshTrainingData = String.Empty;
			rottenTrainingData = String.Empty;
			testData = String.Empty;
			classifer = null;

			loadTestFileBtn.Enabled = false;
			predictReviewBtn.Enabled = false;
		}

		
	}

	public static class ControlExtension {
		public static void InvokeIfRequired(this Control control, Action action) {
			if(control.InvokeRequired) {
				control.Invoke(action);
			}
			else {
				action();
			}
		}
	}


}
