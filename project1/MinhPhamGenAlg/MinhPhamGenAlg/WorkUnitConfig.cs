﻿/**
 * This class is used in auto generating experimental data
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinhPhamGenAlg {
	public class WorkUnitConfig {
		public int NumberChromosomes { get; set; }
		public int GenerationRunTime { get; set; }
		public String SelectAlgorithm { get; set; }
		public double SelectPercentage { get; set; }
		public String CrossoverAlgorithm { get; set; }
		public double MutationRate { get; set; }
		public double DeltaMutationRate { get; set; }
		public double MutationStep { get; set; }
		public bool IsFixedMutationRate { get; set; }
	}
}
