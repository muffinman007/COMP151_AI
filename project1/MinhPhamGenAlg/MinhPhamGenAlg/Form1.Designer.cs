﻿namespace MinhPhamGenAlg {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if(disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
			this.loadFileBtn = new System.Windows.Forms.Button();
			this.chromosomesUpDown = new System.Windows.Forms.NumericUpDown();
			this.numChromLbl = new System.Windows.Forms.Label();
			this.numGenLbl = new System.Windows.Forms.Label();
			this.generationUpDown = new System.Windows.Forms.NumericUpDown();
			this.selectAlgLbl = new System.Windows.Forms.Label();
			this.selectAlgCombox = new System.Windows.Forms.ComboBox();
			this.percentLbl = new System.Windows.Forms.Label();
			this.percentUpDown = new System.Windows.Forms.NumericUpDown();
			this.crossLbl = new System.Windows.Forms.Label();
			this.crossAlgCombox = new System.Windows.Forms.ComboBox();
			this.mutationUpDown = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.deltaMutUpDown = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.fixMutCheckBox = new System.Windows.Forms.CheckBox();
			this.outputTxtBox = new System.Windows.Forms.TextBox();
			this.runBtn = new System.Windows.Forms.Button();
			this.stopSimBtn = new System.Windows.Forms.Button();
			this.simCycleUpDown = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.cycleLabel = new System.Windows.Forms.Label();
			this.autoRunBtn = new System.Windows.Forms.Button();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.genDataBtn = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.chromosomesUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.generationUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.percentUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.mutationUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.deltaMutUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.simCycleUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// openFileDialog1
			// 
			this.openFileDialog1.FileName = "openFileDialog1";
			// 
			// loadFileBtn
			// 
			this.loadFileBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.loadFileBtn.Location = new System.Drawing.Point(15, 12);
			this.loadFileBtn.Name = "loadFileBtn";
			this.loadFileBtn.Size = new System.Drawing.Size(75, 26);
			this.loadFileBtn.TabIndex = 0;
			this.loadFileBtn.Text = "Load File";
			this.loadFileBtn.UseVisualStyleBackColor = true;
			this.loadFileBtn.Click += new System.EventHandler(this.loadFileBtn_Click);
			// 
			// chromosomesUpDown
			// 
			this.chromosomesUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.chromosomesUpDown.Location = new System.Drawing.Point(257, 62);
			this.chromosomesUpDown.Maximum = new decimal(new int[] {
            500000,
            0,
            0,
            0});
			this.chromosomesUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.chromosomesUpDown.Name = "chromosomesUpDown";
			this.chromosomesUpDown.Size = new System.Drawing.Size(80, 21);
			this.chromosomesUpDown.TabIndex = 1;
			this.chromosomesUpDown.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
			// 
			// numChromLbl
			// 
			this.numChromLbl.AutoSize = true;
			this.numChromLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.numChromLbl.ForeColor = System.Drawing.Color.DeepPink;
			this.numChromLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.numChromLbl.Location = new System.Drawing.Point(12, 64);
			this.numChromLbl.Name = "numChromLbl";
			this.numChromLbl.Size = new System.Drawing.Size(239, 15);
			this.numChromLbl.TabIndex = 2;
			this.numChromLbl.Text = "Number of chromosomes each generation";
			this.numChromLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.numChromLbl.UseMnemonic = false;
			// 
			// numGenLbl
			// 
			this.numGenLbl.AutoSize = true;
			this.numGenLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.numGenLbl.ForeColor = System.Drawing.Color.DeepPink;
			this.numGenLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.numGenLbl.Location = new System.Drawing.Point(12, 100);
			this.numGenLbl.Name = "numGenLbl";
			this.numGenLbl.Size = new System.Drawing.Size(167, 15);
			this.numGenLbl.TabIndex = 3;
			this.numGenLbl.Text = "Number of generations to run";
			this.numGenLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// generationUpDown
			// 
			this.generationUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.generationUpDown.Location = new System.Drawing.Point(185, 98);
			this.generationUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.generationUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.generationUpDown.Name = "generationUpDown";
			this.generationUpDown.Size = new System.Drawing.Size(80, 21);
			this.generationUpDown.TabIndex = 4;
			this.generationUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
			// 
			// selectAlgLbl
			// 
			this.selectAlgLbl.AutoSize = true;
			this.selectAlgLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.selectAlgLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.selectAlgLbl.Location = new System.Drawing.Point(12, 136);
			this.selectAlgLbl.Name = "selectAlgLbl";
			this.selectAlgLbl.Size = new System.Drawing.Size(113, 15);
			this.selectAlgLbl.TabIndex = 5;
			this.selectAlgLbl.Text = "Selection algorithm";
			this.selectAlgLbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// selectAlgCombox
			// 
			this.selectAlgCombox.BackColor = System.Drawing.SystemColors.Window;
			this.selectAlgCombox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.selectAlgCombox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.selectAlgCombox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.selectAlgCombox.FormattingEnabled = true;
			this.selectAlgCombox.Items.AddRange(new object[] {
            "Elitist",
            "Tournament"});
			this.selectAlgCombox.Location = new System.Drawing.Point(131, 133);
			this.selectAlgCombox.Name = "selectAlgCombox";
			this.selectAlgCombox.Size = new System.Drawing.Size(134, 23);
			this.selectAlgCombox.TabIndex = 6;
			// 
			// percentLbl
			// 
			this.percentLbl.AutoSize = true;
			this.percentLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.percentLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.percentLbl.Location = new System.Drawing.Point(12, 172);
			this.percentLbl.Name = "percentLbl";
			this.percentLbl.Size = new System.Drawing.Size(209, 15);
			this.percentLbl.TabIndex = 7;
			this.percentLbl.Text = "Selection percentage for propagation";
			// 
			// percentUpDown
			// 
			this.percentUpDown.DecimalPlaces = 2;
			this.percentUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.percentUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.percentUpDown.Location = new System.Drawing.Point(227, 170);
			this.percentUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            65536});
			this.percentUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
			this.percentUpDown.Name = "percentUpDown";
			this.percentUpDown.Size = new System.Drawing.Size(110, 21);
			this.percentUpDown.TabIndex = 8;
			this.percentUpDown.Value = new decimal(new int[] {
            50,
            0,
            0,
            131072});
			// 
			// crossLbl
			// 
			this.crossLbl.AutoSize = true;
			this.crossLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.crossLbl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.crossLbl.Location = new System.Drawing.Point(12, 208);
			this.crossLbl.Name = "crossLbl";
			this.crossLbl.Size = new System.Drawing.Size(116, 15);
			this.crossLbl.TabIndex = 9;
			this.crossLbl.Text = "Crossover algorithm";
			// 
			// crossAlgCombox
			// 
			this.crossAlgCombox.BackColor = System.Drawing.SystemColors.Window;
			this.crossAlgCombox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.crossAlgCombox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.crossAlgCombox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.crossAlgCombox.FormattingEnabled = true;
			this.crossAlgCombox.Items.AddRange(new object[] {
            "Uniform",
            "Kpoint"});
			this.crossAlgCombox.Location = new System.Drawing.Point(131, 205);
			this.crossAlgCombox.Name = "crossAlgCombox";
			this.crossAlgCombox.Size = new System.Drawing.Size(134, 23);
			this.crossAlgCombox.TabIndex = 10;
			// 
			// mutationUpDown
			// 
			this.mutationUpDown.DecimalPlaces = 4;
			this.mutationUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.mutationUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            262144});
			this.mutationUpDown.Location = new System.Drawing.Point(131, 242);
			this.mutationUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            65536});
			this.mutationUpDown.Name = "mutationUpDown";
			this.mutationUpDown.Size = new System.Drawing.Size(134, 21);
			this.mutationUpDown.TabIndex = 12;
			this.mutationUpDown.Value = new decimal(new int[] {
            50,
            0,
            0,
            131072});
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.label1.Location = new System.Drawing.Point(12, 244);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(111, 15);
			this.label1.TabIndex = 11;
			this.label1.Text = "Initial mutation rate";
			// 
			// deltaMutUpDown
			// 
			this.deltaMutUpDown.DecimalPlaces = 7;
			this.deltaMutUpDown.Enabled = false;
			this.deltaMutUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.deltaMutUpDown.Increment = new decimal(new int[] {
            1,
            0,
            0,
            458752});
			this.deltaMutUpDown.Location = new System.Drawing.Point(131, 278);
			this.deltaMutUpDown.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            65536});
			this.deltaMutUpDown.Name = "deltaMutUpDown";
			this.deltaMutUpDown.Size = new System.Drawing.Size(134, 21);
			this.deltaMutUpDown.TabIndex = 14;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.label2.Location = new System.Drawing.Point(12, 280);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(111, 15);
			this.label2.TabIndex = 13;
			this.label2.Text = "Delta mutation rate";
			// 
			// fixMutCheckBox
			// 
			this.fixMutCheckBox.AutoSize = true;
			this.fixMutCheckBox.Checked = true;
			this.fixMutCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.fixMutCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.fixMutCheckBox.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.fixMutCheckBox.Location = new System.Drawing.Point(15, 316);
			this.fixMutCheckBox.Name = "fixMutCheckBox";
			this.fixMutCheckBox.Size = new System.Drawing.Size(131, 19);
			this.fixMutCheckBox.TabIndex = 15;
			this.fixMutCheckBox.Text = "Fixed mutation rate";
			this.fixMutCheckBox.UseVisualStyleBackColor = true;
			this.fixMutCheckBox.CheckedChanged += new System.EventHandler(this.fixMutCheckBox_CheckedChanged);
			// 
			// outputTxtBox
			// 
			this.outputTxtBox.BackColor = System.Drawing.Color.White;
			this.outputTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.outputTxtBox.Location = new System.Drawing.Point(374, 12);
			this.outputTxtBox.Multiline = true;
			this.outputTxtBox.Name = "outputTxtBox";
			this.outputTxtBox.ReadOnly = true;
			this.outputTxtBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.outputTxtBox.Size = new System.Drawing.Size(252, 457);
			this.outputTxtBox.TabIndex = 20;
			this.outputTxtBox.Text = "Please load in the data file.";
			// 
			// runBtn
			// 
			this.runBtn.BackColor = System.Drawing.Color.PaleGreen;
			this.runBtn.Enabled = false;
			this.runBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.runBtn.Location = new System.Drawing.Point(100, 441);
			this.runBtn.Name = "runBtn";
			this.runBtn.Size = new System.Drawing.Size(110, 28);
			this.runBtn.TabIndex = 18;
			this.runBtn.Text = "Run Simulation";
			this.runBtn.UseVisualStyleBackColor = false;
			this.runBtn.Click += new System.EventHandler(this.runBtn_Click);
			// 
			// stopSimBtn
			// 
			this.stopSimBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
			this.stopSimBtn.Enabled = false;
			this.stopSimBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.stopSimBtn.Location = new System.Drawing.Point(12, 441);
			this.stopSimBtn.Name = "stopSimBtn";
			this.stopSimBtn.Size = new System.Drawing.Size(55, 28);
			this.stopSimBtn.TabIndex = 19;
			this.stopSimBtn.Text = "Stop";
			this.stopSimBtn.UseVisualStyleBackColor = false;
			this.stopSimBtn.Click += new System.EventHandler(this.stopSimBtn_Click);
			// 
			// simCycleUpDown
			// 
			this.simCycleUpDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.simCycleUpDown.Location = new System.Drawing.Point(173, 350);
			this.simCycleUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.simCycleUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.simCycleUpDown.Name = "simCycleUpDown";
			this.simCycleUpDown.Size = new System.Drawing.Size(80, 21);
			this.simCycleUpDown.TabIndex = 16;
			this.simCycleUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.ForeColor = System.Drawing.Color.DeepPink;
			this.label3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.label3.Location = new System.Drawing.Point(12, 352);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(155, 15);
			this.label3.TabIndex = 20;
			this.label3.Text = "Number of simulation cycle";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cycleLabel
			// 
			this.cycleLabel.AutoSize = true;
			this.cycleLabel.Location = new System.Drawing.Point(259, 354);
			this.cycleLabel.Name = "cycleLabel";
			this.cycleLabel.Size = new System.Drawing.Size(82, 13);
			this.cycleLabel.TabIndex = 21;
			this.cycleLabel.Text = "Current Cycle: 0";
			// 
			// autoRunBtn
			// 
			this.autoRunBtn.BackColor = System.Drawing.Color.Violet;
			this.autoRunBtn.Enabled = false;
			this.autoRunBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.autoRunBtn.Location = new System.Drawing.Point(244, 441);
			this.autoRunBtn.Name = "autoRunBtn";
			this.autoRunBtn.Size = new System.Drawing.Size(93, 28);
			this.autoRunBtn.TabIndex = 22;
			this.autoRunBtn.Text = "Auto Run";
			this.autoRunBtn.UseVisualStyleBackColor = false;
			this.autoRunBtn.Click += new System.EventHandler(this.autoRunBtn_Click);
			// 
			// saveFileDialog1
			// 
			this.saveFileDialog1.DefaultExt = "txt";
			this.saveFileDialog1.FileName = "genAlg_Debug_Traning_Data";
			this.saveFileDialog1.Filter = "Text File | *.txt";
			// 
			// genDataBtn
			// 
			this.genDataBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.genDataBtn.Location = new System.Drawing.Point(232, 12);
			this.genDataBtn.Name = "genDataBtn";
			this.genDataBtn.Size = new System.Drawing.Size(109, 26);
			this.genDataBtn.TabIndex = 23;
			this.genDataBtn.Text = "Generate Data";
			this.genDataBtn.UseVisualStyleBackColor = true;
			this.genDataBtn.Click += new System.EventHandler(this.genDataBtn_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(638, 481);
			this.Controls.Add(this.genDataBtn);
			this.Controls.Add(this.autoRunBtn);
			this.Controls.Add(this.cycleLabel);
			this.Controls.Add(this.simCycleUpDown);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.stopSimBtn);
			this.Controls.Add(this.runBtn);
			this.Controls.Add(this.outputTxtBox);
			this.Controls.Add(this.fixMutCheckBox);
			this.Controls.Add(this.deltaMutUpDown);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.mutationUpDown);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.crossAlgCombox);
			this.Controls.Add(this.crossLbl);
			this.Controls.Add(this.percentUpDown);
			this.Controls.Add(this.percentLbl);
			this.Controls.Add(this.selectAlgCombox);
			this.Controls.Add(this.selectAlgLbl);
			this.Controls.Add(this.generationUpDown);
			this.Controls.Add(this.numGenLbl);
			this.Controls.Add(this.numChromLbl);
			this.Controls.Add(this.chromosomesUpDown);
			this.Controls.Add(this.loadFileBtn);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximumSize = new System.Drawing.Size(654, 520);
			this.MinimumSize = new System.Drawing.Size(654, 520);
			this.Name = "Form1";
			this.Text = "Project 1 Genetic Algorithm";
			((System.ComponentModel.ISupportInitialize)(this.chromosomesUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.generationUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.percentUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.mutationUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.deltaMutUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.simCycleUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog openFileDialog1;
		private System.Windows.Forms.Button loadFileBtn;
		private System.Windows.Forms.NumericUpDown chromosomesUpDown;
		private System.Windows.Forms.Label numChromLbl;
		private System.Windows.Forms.Label numGenLbl;
		private System.Windows.Forms.NumericUpDown generationUpDown;
		private System.Windows.Forms.Label selectAlgLbl;
		public System.Windows.Forms.ComboBox selectAlgCombox;
		private System.Windows.Forms.Label percentLbl;
		private System.Windows.Forms.NumericUpDown percentUpDown;
		private System.Windows.Forms.Label crossLbl;
		public System.Windows.Forms.ComboBox crossAlgCombox;
		private System.Windows.Forms.NumericUpDown mutationUpDown;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown deltaMutUpDown;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox fixMutCheckBox;
		private System.Windows.Forms.TextBox outputTxtBox;
		private System.Windows.Forms.Button runBtn;
		private System.Windows.Forms.Button stopSimBtn;
		private System.Windows.Forms.NumericUpDown simCycleUpDown;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label cycleLabel;
		private System.Windows.Forms.Button autoRunBtn;
		private System.Windows.Forms.SaveFileDialog saveFileDialog1;
		private System.Windows.Forms.Button genDataBtn;
	}
}

