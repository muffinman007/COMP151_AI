﻿/**
 * Buy or short coding: Buy = 1		Short = 0
 * 
 * Chromsome characteristics:
 *			[ 1st day change lower range, 1st day change upper range, 2nd day change lower range, 2nd day change upper range, buy or short recommendation]
 * 
 * 
 * Fitness Function: 
 *		- Each chromosome:
 *				- sweep through all the historical data in the data file  (raw data)
 *					- if the historical data match the patterns in the chromosome
 *						- compute the gain/loss and add it to the running total
 *						
 *					- if chromosome has no match
 *						- assign a fitness of -5000
 * 
 * formula for the gross profit relative to the buy or sell recommendation:
 *		gross += (BUY_SHORT == 1 ? PROFIT : -1 * PROFIT)
 */



using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using TestSimpleRNG;

namespace MinhPhamGenAlg {
	public partial class Form1 : Form {

		
		private List<List<double>> rawDataList = new List<List<double>>();
		private List<List<double>> populationList = new List<List<double>>();

		// keys are the index to populationList, double is the fitness score
		private Dictionary<int, double> fitnessDic = new Dictionary<int, double>();

		// the inner class holds [fitness value, the 5 chromosome properties]
		private List<List<double>> cycleData = new List<List<double>>();

		const int CHROMOSOME_SIZE = 5;		

		const string outputFilename = "GenAlg_ExperimentalData.cvs";

		enum ChromIndex {
			FIRST_RANGE_LOWER,
			FIRST_RANGE_UPPER,
			SECOND_RANGE_LOWER,
			SECOND_RANGE_UPPER,
			BUY_SHORT
		}

		enum RawIndex {
			FIRST_PRICE_DELTA,		// first percentage price change
			SECOND_PRICE_DELTA,			// second percentage price change
			PROFIT						// profit in dollars
		}


		int	numOfChromosomes;			// number of chromosomes each generation, used in auto run as well
		int generationRunTime;			// how many generation to run, used in auto run as well
		string selectAlgorithm;
		double selectPercentage;
		string crossoverAlgorithm;
		double mutationRate;
		double deltaMutationRate;       // effect mutationRate but mutationRate never goes to zero
		double mutationStep;			// how much to decrease the mutation by
		bool isFixedMutationRate;

		int numOfSimCycle;				// used in auto run as well
		bool isRunningMultCycle = false;

		bool isStopSimulation;


		public Form1() {
			InitializeComponent();

			selectAlgCombox.SelectedIndex	= 0;
			crossAlgCombox.SelectedIndex	= 0;

			SimpleRNG.SetSeedFromSystemTime();
		}


		// first: user must load in the file
		private async void loadFileBtn_Click(object sender, EventArgs e) {
			rawDataList.Clear();
			if(openFileDialog1.ShowDialog() == DialogResult.OK) {				
				await Task.Run(() => {		
					// read in the file 
					using(StreamReader sr = new StreamReader(openFileDialog1.OpenFile())) {
						Double d;
						String str = sr.ReadLine();
						if(str == null) {
							outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.Text = "ERROR: File contain invalid data"; });
							runBtn.InvokeIfRequired(()=> { runBtn.Enabled = false; });
							return;
						}

						// read the first line and check if it's the right file
						// the right file should have 3 data per line
						String[] tokens = str.Split(new char[] {' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);

						if(tokens.Length > 3 || !Double.TryParse(tokens[0], out d)) {
							// wrong data format
							outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.Text = "ERROR: File contain invalid data"; });
							runBtn.InvokeIfRequired(()=> { runBtn.Enabled = false; });
							return;
						}

						// file has the correct data format, start loading in the data 
						rawDataList.Add(new List<double>() { Double.Parse(tokens[0]),  Double.Parse(tokens[1]),  Double.Parse(tokens[2]) });

						while((str = sr.ReadLine()) != null) {
							tokens = str.Split(new char[] {' ', '\t' }, StringSplitOptions.RemoveEmptyEntries);
							rawDataList.Add(new List<double>() { Double.Parse(tokens[0]),  Double.Parse(tokens[1]),  Double.Parse(tokens[2]) });
						}
					}	
					
					outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.Text = "Data file loaded succesfully."; });
					// turn run simulation button on
					runBtn.InvokeIfRequired(()=> { runBtn.Enabled = true; });
					autoRunBtn.InvokeIfRequired(()=> { autoRunBtn.Enabled = true; });
				});
			}			
		}

		// each run is a new simulation run
		private void runBtn_Click(object sender, EventArgs e) {
			numOfChromosomes	= (int)chromosomesUpDown.Value;
			generationRunTime	= (int)generationUpDown.Value;
			selectAlgorithm		= selectAlgCombox.Text;
			selectPercentage	= (double)percentUpDown.Value;
			crossoverAlgorithm	= crossAlgCombox.Text;
			mutationRate		= (double)mutationUpDown.Value;
			deltaMutationRate	= (double)deltaMutUpDown.Value;
			isFixedMutationRate	= fixMutCheckBox.Checked;
			numOfSimCycle		= (int)simCycleUpDown.Value;
			isRunningMultCycle	= false;

			if(numOfSimCycle > 1) {
				isRunningMultCycle = true;
				cycleData.Clear();
			}

			/*
			  The initial mutation rate and how much to change the mutation rate each generation – it
			  should be possible to have a fixed mutation rate or to have a mutation rate that gradually
			  decreases (without reaching zero)

			  mutationStep would be subtracting factor to gradually decrease the mutation rate without it ever reaching zero.
			*/
			mutationStep		= (mutationRate * deltaMutationRate) / (generationRunTime * 2);

			outputTxtBox.Text	= "Simulation is running...";
			loadFileBtn.Enabled = false;
			runBtn.Enabled		= false;
			genDataBtn.Enabled	= false;

			stopSimBtn.Enabled	= true;
			isStopSimulation	= false;

			Task.Run(()=> {
				
				int cycleCounter = 1;
				while(cycleCounter <= numOfSimCycle) {

					cycleLabel.InvokeIfRequired(()=> { cycleLabel.Text = "Current Cycle: " + cycleCounter; });

					// generating population
					GeneratePopulation();
				
					int i = 0;
					while(i < generationRunTime) {

						FitnessTest();

						Selection();

						Crossover();

						Mutation();				

						++i;

						if(isStopSimulation) {
							PrintFittest(i, isRunningMultCycle);
							break;
						}

						// print current population status
						if( (i != 0 && i % 10 == 0) || (i == generationRunTime)) {	// print out the last report if generationRunTime is not multiple of 10
							// displaying max, min, mean fitness of the chromosomes 
							outputTxtBox.InvokeIfRequired(()=> {
								outputTxtBox.AppendText(
									Environment.NewLine + Environment.NewLine + "Generation: " + i + Environment.NewLine +
									"Fitness Statistics:" + Environment.NewLine +
									"Max: " + fitnessDic.Values.Max() + Environment.NewLine +
									"Min: " + fitnessDic.Values.Min() + Environment.NewLine +
									"Mean: " + fitnessDic.Values.Sum()/fitnessDic.Count
								);
							});

							// if it's the last generation
							// find the highest fitness chromosome and display the chromosome to the screen.
							if(i == generationRunTime) {
								PrintFittest(i, isRunningMultCycle);
							}
						}
					}

					if(isStopSimulation) break;

					++cycleCounter;
				}

				if(cycleData.Count > 1) {
					// find the most fitness
					var sorted = from dlist in cycleData
								 orderby dlist[0] descending
								 select dlist;

					outputTxtBox.InvokeIfRequired(()=> {
						outputTxtBox.AppendText(
							Environment.NewLine + Environment.NewLine + "Cycling Result: " + Environment.NewLine +
							"Chromosome Characteristic:" + Environment.NewLine +
							"Fitness Value: " + sorted.First()[0] + Environment.NewLine + 
							"Genome: " +  Environment.NewLine +
							"[" + Environment.NewLine + 
							"\t" + sorted.First()[1] + ", " + Environment.NewLine + 
							"\t" + sorted.First()[2] + ", " + Environment.NewLine + 
							"\t" + sorted.First()[3] + ", " + Environment.NewLine +  
							"\t" + sorted.First()[4] + ", " + Environment.NewLine + 
							"\t" + sorted.First()[5] + Environment.NewLine + 
							"]" +	Environment.NewLine	
						);
					});
				}

				outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Simulation ended successfully."); });
				runBtn.InvokeIfRequired(()=> { runBtn.Enabled = true; });
				loadFileBtn.InvokeIfRequired(()=> { loadFileBtn.Enabled = true; });
				stopSimBtn.InvokeIfRequired(()=> { stopSimBtn.Enabled = false; });
				genDataBtn.InvokeIfRequired(()=> { genDataBtn.Enabled = true; });
			});
		}


		void PrintFittest(int i, bool isSimCycling) {
			FitnessTest();

			var fittest = from pair in fitnessDic
						  orderby pair.Value descending
						  select pair;

			var winner = fittest.First();

			outputTxtBox.InvokeIfRequired(()=> {
				outputTxtBox.AppendText(
					Environment.NewLine + Environment.NewLine + "Final Generation: " + i + Environment.NewLine +
					"Chromosome Characteristic:" + Environment.NewLine +
					"Fitness Value: " + winner.Value + Environment.NewLine + 
					"Genome: " +  Environment.NewLine +
					"[" + Environment.NewLine + "\t" + populationList[winner.Key][0] + ", " + Environment.NewLine + 
					"\t" + populationList[winner.Key][1] + ", " + Environment.NewLine + 
					"\t" + populationList[winner.Key][2] + ", " + Environment.NewLine +  
					"\t" + populationList[winner.Key][3] + ", " + Environment.NewLine + 
					"\t" + populationList[winner.Key][4] + Environment.NewLine + 
					"]" +	Environment.NewLine	
				);
			});

			if(isSimCycling) {
				cycleData.Add(new List<double>() { winner.Value, populationList[winner.Key][0], populationList[winner.Key][1], populationList[winner.Key][2], populationList[winner.Key][3], populationList[winner.Key][4]});
			}
		}


		void GeneratePopulation() {
			populationList.Clear();

			int i = 0;
			while(i < numOfChromosomes) {
				// create a chromosome
				populationList.Add(new List<double>(CHROMOSOME_SIZE) { 0.0, 0.0, 0.0, 0.0, 0.0 });
				populationList[populationList.Count - 1][(int)ChromIndex.FIRST_RANGE_LOWER] = SimpleRNG.GetNormal(0.0, 1.15);
				populationList[populationList.Count - 1][(int)ChromIndex.FIRST_RANGE_UPPER] = SimpleRNG.GetNormal(0.0, 1.15);
				populationList[populationList.Count - 1][(int)ChromIndex.SECOND_RANGE_LOWER] = SimpleRNG.GetNormal(0.0, 1.15);
				populationList[populationList.Count - 1][(int)ChromIndex.SECOND_RANGE_UPPER] = SimpleRNG.GetNormal(0.0, 1.15);
				populationList[populationList.Count - 1][(int)ChromIndex.BUY_SHORT] = Math.Round(SimpleRNG.GetUniform());

				// rearrange
				if(populationList[populationList.Count - 1][(int)ChromIndex.FIRST_RANGE_LOWER] > populationList[populationList.Count - 1][(int)ChromIndex.FIRST_RANGE_UPPER]) {
					double temp = populationList[populationList.Count - 1][(int)ChromIndex.FIRST_RANGE_LOWER];
					populationList[populationList.Count - 1][(int)ChromIndex.FIRST_RANGE_LOWER] = populationList[populationList.Count - 1][(int)ChromIndex.FIRST_RANGE_UPPER];
					populationList[populationList.Count - 1][(int)ChromIndex.FIRST_RANGE_UPPER] = temp;
				}
				if(populationList[populationList.Count - 1][(int)ChromIndex.SECOND_RANGE_LOWER] > populationList[populationList.Count - 1][(int)ChromIndex.SECOND_RANGE_UPPER]) {
					double temp = populationList[populationList.Count - 1][(int)ChromIndex.SECOND_RANGE_LOWER];
					populationList[populationList.Count - 1][(int)ChromIndex.SECOND_RANGE_LOWER] = populationList[populationList.Count - 1][(int)ChromIndex.SECOND_RANGE_UPPER];
					populationList[populationList.Count - 1][(int)ChromIndex.SECOND_RANGE_UPPER] = temp;
				}

				++i;
			}
		}

		void FitnessTest() {
			double profit;			// total profit
			bool noMatchFound;
			fitnessDic.Clear();
			
			for(int i = 0; i < populationList.Count; ++i) {
				profit = 0.0;
				noMatchFound = true;
				foreach(var data in rawDataList) {
					if( (data[(int)RawIndex.FIRST_PRICE_DELTA]  >= populationList[i][(int)ChromIndex.FIRST_RANGE_LOWER])  &&
						(data[(int)RawIndex.FIRST_PRICE_DELTA]  <= populationList[i][(int)ChromIndex.FIRST_RANGE_UPPER])  &&
						(data[(int)RawIndex.SECOND_PRICE_DELTA] >= populationList[i][(int)ChromIndex.SECOND_RANGE_LOWER]) && 
						(data[(int)RawIndex.SECOND_PRICE_DELTA] <= populationList[i][(int)ChromIndex.SECOND_RANGE_UPPER])
					) {
						// gross += (BUY_SHORT == 1 ? PROFIT : -1 * PROFIT)
						profit += (populationList[i][(int)ChromIndex.BUY_SHORT] == 1? data[(int)RawIndex.PROFIT] : -1 * data[(int)RawIndex.PROFIT]);
						noMatchFound = false;
					}
				}

				if(noMatchFound) {
					fitnessDic.Add(i, -5000);
				}
				else {
					fitnessDic.Add(i, profit);
				}
			}
		}
		
		void Selection() {
			List<List<double>> oldPop = populationList;
			populationList = new List<List<double>>();
			
			int numChildren = (int)Math.Round(oldPop.Count * selectPercentage);

			if(selectAlgorithm == "Elitist") {
				// order the population according to the highest fitness
				var fittest = from pair in fitnessDic
							  orderby pair.Value descending
						      select pair;
				
				foreach(var pair in fittest) {
					populationList.Add(oldPop[pair.Key]);
					
					if((--numChildren) == 0) {
						break;
					}
				}				 
			}
			else if(selectAlgorithm == "Tournament") {
				int i;
				int j;

				while(numChildren > 0) {
					i = (int)Math.Round(SimpleRNG.GetUniform() * (fitnessDic.Count - 1));
					do {
						j = (int)Math.Round(SimpleRNG.GetUniform() * (fitnessDic.Count - 1));
					}while(j == i);
					
					if(fitnessDic[i] > fitnessDic[j]) {
						populationList.Add(oldPop[i]);						
					}
					else {
						populationList.Add(oldPop[j]);
					}

					--numChildren;
				}
			}
		}

		void Crossover() {
			int makeBabies = numOfChromosomes - populationList.Count;

			int indexLimit = populationList.Count - 1;
			int i, j, k;

			while(makeBabies > 0) {
				i = (int)Math.Round(SimpleRNG.GetUniform() * indexLimit);
				do {
					j = (int)Math.Round(SimpleRNG.GetUniform() * indexLimit);
				}while(j == i);

				List<double> baby = new List<double>(CHROMOSOME_SIZE) { 0.0, 0.0, 0.0, 0.0, 0.0 };

				if(crossoverAlgorithm == "Uniform") {
					for(k = 0; k < 5; ++k) {
						switch((int)Math.Round(SimpleRNG.GetUniform())) {
							case 0:
								baby[k] = populationList[i][k];
								break;
							case 1:
								baby[k] = populationList[j][k];
								break;
						}
					}

					if(baby[0] > baby[1]) {
						double temp = baby[0];
						baby[0] = baby[1];
						baby[1] = temp;
					}
					if(baby[2] > baby[3]) {
						double temp = baby[2];
						baby[2] = baby[3];
						baby[3] = temp;
					}
				}
				else if(crossoverAlgorithm == "Kpoint") {
					baby[0] = populationList[i][0];
					baby[1] = populationList[i][1];
					baby[2] = populationList[j][2];
					baby[3] = populationList[j][3];
					baby[4] = populationList[j][4];
				}

				populationList.Add(baby);

				--makeBabies;
			}
		}

		void Mutation() {	
			foreach(var chromosome in populationList) {
				chromosome[(int)ChromIndex.FIRST_RANGE_LOWER]	= SimpleRNG.GetUniform() <= mutationRate ? SimpleRNG.GetNormal(0.0, 1.15)	  : chromosome[(int)ChromIndex.FIRST_RANGE_LOWER];
				chromosome[(int)ChromIndex.FIRST_RANGE_UPPER]	= SimpleRNG.GetUniform() <= mutationRate ? SimpleRNG.GetNormal(0.0, 1.15)	  : chromosome[(int)ChromIndex.FIRST_RANGE_UPPER];
				chromosome[(int)ChromIndex.SECOND_RANGE_LOWER]	= SimpleRNG.GetUniform() <= mutationRate ? SimpleRNG.GetNormal(0.0, 1.15)	  : chromosome[(int)ChromIndex.SECOND_RANGE_LOWER];
				chromosome[(int)ChromIndex.SECOND_RANGE_UPPER]	= SimpleRNG.GetUniform() <= mutationRate ? SimpleRNG.GetNormal(0.0, 1.15)	  : chromosome[(int)ChromIndex.SECOND_RANGE_LOWER];
				chromosome[(int)ChromIndex.BUY_SHORT]			= SimpleRNG.GetUniform() <= mutationRate ? Math.Round(SimpleRNG.GetUniform()) : chromosome[(int)ChromIndex.BUY_SHORT];

				if(chromosome[(int)ChromIndex.FIRST_RANGE_LOWER] > chromosome[(int)ChromIndex.FIRST_RANGE_UPPER]) {
					double temp = chromosome[(int)ChromIndex.FIRST_RANGE_LOWER];
					chromosome[(int)ChromIndex.FIRST_RANGE_LOWER] = chromosome[(int)ChromIndex.FIRST_RANGE_UPPER];
					chromosome[(int)ChromIndex.FIRST_RANGE_UPPER] = temp;
				}
				if(chromosome[(int)ChromIndex.SECOND_RANGE_LOWER] > chromosome[(int)ChromIndex.SECOND_RANGE_UPPER]) {
					double temp = chromosome[(int)ChromIndex.SECOND_RANGE_LOWER];
					chromosome[(int)ChromIndex.SECOND_RANGE_LOWER] = chromosome[(int)ChromIndex.SECOND_RANGE_UPPER];
					chromosome[(int)ChromIndex.SECOND_RANGE_UPPER] = temp;
				}
			}
				
			if(!isFixedMutationRate) {
				// recalcualte mutation rate for next generation
				mutationRate -= mutationStep;
			}
		}


		private void fixMutCheckBox_CheckedChanged(object sender, EventArgs e) {
			if(fixMutCheckBox.Checked) {
				deltaMutUpDown.Enabled = false;
			}
			else {
				deltaMutUpDown.Enabled = true;
			}
		}

		private void stopSimBtn_Click(object sender, EventArgs e) {
			isStopSimulation = true;
		}

		private async void autoRunBtn_Click(object sender, EventArgs e) {
			// turn everything off and run auto simulation
			loadFileBtn.Enabled = false;
			stopSimBtn.Enabled = false;
			runBtn.Enabled = false;
			autoRunBtn.Enabled = false;
			genDataBtn.Enabled = false;

			outputTxtBox.Text = "Auto running the simulation.\n";

			numOfChromosomes	= (int)chromosomesUpDown.Value;
			generationRunTime	= (int)generationUpDown.Value;
			numOfSimCycle		= (int)simCycleUpDown.Value;

			await Task.Run(()=> {
				List<WorkUnitConfig> workList = new List<WorkUnitConfig>();
				string selection = "Elitist";
				for(int select = 0; select < 2; ++select) {
					if(select == 1) {
						selection = "Tournament";
					}

					// generating the workUnit
					for(double percentage = 0.10; percentage <= 1.0; percentage += 0.10) {
						for(double mutation = 0.0; mutation <= 1.0f; mutation += 0.10) {
							workList.Add(
								new WorkUnitConfig() {
									NumberChromosomes	= numOfChromosomes,
									GenerationRunTime	= generationRunTime,
									SelectAlgorithm		= selection,
									SelectPercentage	= percentage,
									CrossoverAlgorithm	= "Uniform",
									MutationRate		= mutation,
									DeltaMutationRate	= 0.0,
									MutationStep		= 0.0, 
									IsFixedMutationRate = true
								}							
							);
							workList.Add(
								new WorkUnitConfig() {
									NumberChromosomes	= numOfChromosomes,
									GenerationRunTime	= generationRunTime,
									SelectAlgorithm		= selection,
									SelectPercentage	= percentage,
									CrossoverAlgorithm	= "Kpoint",
									MutationRate		= mutation,
									DeltaMutationRate	= 0.0,
									MutationStep		= 0.0, 
									IsFixedMutationRate = true
								}							
							);
						}
					}
				}

				// file header
				using(StreamWriter sw = new StreamWriter(outputFilename, true)) {
					sw.WriteLine(
						"num_Chromosomes,generation,selectAlgorithm,selectPercentage,crossoverAlgorithm,mutationRate,deltaMutationRate,isFixedMutationRate,mutationStep,fitnessValue,first_lower,first_upper,second_lower,second_higher,buy_sell"
					);
				}

				int workUnitCounter = 1;

				// run the simulation
				foreach(var workUnit in workList) {
					numOfChromosomes	= workUnit.NumberChromosomes;
					generationRunTime	= workUnit.GenerationRunTime;
					selectAlgorithm		= workUnit.SelectAlgorithm;
					selectPercentage	= workUnit.SelectPercentage;
					crossoverAlgorithm	= workUnit.CrossoverAlgorithm;
					mutationRate		= workUnit.MutationRate;
					deltaMutationRate	= workUnit.DeltaMutationRate;
					isFixedMutationRate	= workUnit.IsFixedMutationRate;
					mutationStep		= workUnit.MutationStep;

					int workUnitCycle = 0;
					while(workUnitCycle < numOfSimCycle) {

						outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Work Unit: " + workUnitCounter + " Cycle: " + (workUnitCycle + 1) + Environment.NewLine); });

						GeneratePopulation();

						int genCounter = 0;
						while(genCounter < generationRunTime) {
							FitnessTest();
							Selection();
							Crossover();
							Mutation();

							++genCounter;

							if(genCounter == generationRunTime) {
								// record data
								var fittest = from pair in fitnessDic
											  orderby pair.Value descending
											  select pair;
								var winner = fittest.First();
																
								using(StreamWriter sw = new StreamWriter(outputFilename, true)) {
									sw.WriteLine(
										numOfChromosomes	+ "," +
										generationRunTime	+ "," +
										selectAlgorithm		+ "," +
										selectPercentage	+ "," +
										crossoverAlgorithm	+ "," +
										mutationRate		+ "," +
										deltaMutationRate	+ "," +
										isFixedMutationRate	+ "," +
										mutationStep		+ "," +
										winner.Value		+ "," +
										populationList[winner.Key][0] + "," +
										populationList[winner.Key][1] + "," +
										populationList[winner.Key][2] + "," +
										populationList[winner.Key][3] + "," +
										populationList[winner.Key][4]
									);
								}

								outputTxtBox.InvokeIfRequired(()=> { outputTxtBox.AppendText("Finish writing data to file." + Environment.NewLine); });
							}
						} 

						++workUnitCycle;
					} // work unit cycle

					++workUnitCounter;
				} // work unit
			}); // auto run

			outputTxtBox.AppendText("Auto run successfully.\nData saved to file.\n");

			loadFileBtn.Enabled = true;
			stopSimBtn.Enabled = true;
			runBtn.Enabled = true;
			autoRunBtn.Enabled = true;
			genDataBtn.Enabled	= true;
		}
		
		private async void genDataBtn_Click(object sender, EventArgs e) {
			bool runBtnState	= runBtn.Enabled;
			bool autoBtnState	= autoRunBtn.Enabled;
			loadFileBtn.Enabled = false;
			genDataBtn.Enabled	= false;
			
			outputTxtBox.Text = "Generating debug data..." + Environment.NewLine;
			StringBuilder sb = new StringBuilder();

			await Task.Run(()=> {
				// the number 7564 comes from genAlgData1.txt
				for(int i = 0; i < 7564; ++i) {
					sb.AppendLine(
						Math.Round(SimpleRNG.GetNormal(-0.023480962f, 1.169556345f), 2) + "\t" +
						Math.Round(SimpleRNG.GetNormal(-0.023470386f, 1.169555834f), 2) + "\t" +
						Math.Round(SimpleRNG.GetNormal(1.194972237f, 11.01922961f),  2)
					);
				}				
			});

			outputTxtBox.AppendText("Saving debug data..." + Environment.NewLine);

			if(saveFileDialog1.ShowDialog() == DialogResult.OK) {
				File.WriteAllText(saveFileDialog1.FileName, sb.ToString());
			}

			outputTxtBox.AppendText("Finish saving debug data to file." + Environment.NewLine + "Please load in a data file.");

			runBtn.Enabled		= runBtnState;
			autoRunBtn.Enabled	= autoBtnState;
			loadFileBtn.Enabled = true;
			genDataBtn.Enabled	= true;
		}






	}// end form



	public static class ControlExtension {
		public static void InvokeIfRequired(this Control control, Action action) {
			if(control.InvokeRequired) {
				control.Invoke(action);
			}
			else {
				action();
			}
		}
	}
}
