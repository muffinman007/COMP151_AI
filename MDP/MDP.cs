﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MDP : MonoBehaviour {

	// UI - Value
	public Text s1ValueText;
	public Text s2ValueText;
	public Text s3ValueText;

	// UI - Policy
	public Text s1PolicyText;
	public Text s2PolicyText;
	public Text s3PolicyText;

	// UI
	public Text iterationStepText;

	#region Probabilities and Reward
	// reward and cost "function"
	Dictionary<string, float> rewardCostDic = new Dictionary<string, float>(7) {
		{"s1", -0.1f },
		{"s2", -0.1f },
		{"s3", -0.1f },
		{"s4", -4.0f },
		{"s5",  2.0f },
		{"s6",  5.0f },
		{"s7", -2.0f }
	};

	// transition probabilities for walk
	Dictionary<string, Dictionary<string, float>> walkTransitionProb = new Dictionary<string, Dictionary<string, float>>(3) {
		{"s1", new Dictionary<string, float>(7) 
			{
				{"s1", 0.5f },
				{"s2", 0.5f },
				{"s3", 0.0f },
				{"s4", 0.0f },
				{"s5", 0.0f },
				{"s6", 0.0f },
				{"s7", 0.0f }
			}
		},
		{"s2", new Dictionary<string, float>(7) 
			{
				{"s1", 0.0f },
				{"s2", 0.0f },
				{"s3", 0.0f },
				{"s4", 0.5f },
				{"s5", 0.5f },
				{"s6", 0.0f },
				{"s7", 0.0f }
			}
		},
		{"s3", new Dictionary<string, float>(7) 
			{
				{"s1", 0.0f },
				{"s2", 0.0f },
				{"s3", 0.5f },
				{"s4", 0.0f },
				{"s5", 0.0f },
				{"s6", 0.5f },
				{"s7", 0.0f }
			}
		}
	};
	
	// transition probabilities for teleport
	Dictionary<string, Dictionary<string, float>> teleportTransitionProb = new Dictionary<string, Dictionary<string, float>>(3) {
		{"s1", new Dictionary<string, float>(7) 
			{
				{"s1", 0.0f },
				{"s2", 0.5f },
				{"s3", 0.5f },
				{"s4", 0.0f },
				{"s5", 0.0f },
				{"s6", 0.0f },
				{"s7", 0.0f }
			}
		},
		{"s2", new Dictionary<string, float>(7) 
			{
				{"s1", 0.0f },
				{"s2", 0.0f },
				{"s3", 0.25f },
				{"s4", 0.75f },
				{"s5", 0.0f },
				{"s6", 0.0f },
				{"s7", 0.0f }
			}
		},
		{"s3", new Dictionary<string, float>(7) 
			{
				{"s1", 0.0f },
				{"s2", 0.0f },
				{"s3", 0.0f },
				{"s4", 0.0f },
				{"s5", 0.0f },
				{"s6", 0.5f },
				{"s7", 0.5f }
			}
		}
	};
	#endregion

	class StateData {
		public string policy = string.Empty;
		public float value;

		public StateData(string policy, float value) {
			this.policy = policy;
			this.value = value;
		}
	}

	List<StateData> s1ValueList = new List<StateData>() { new StateData("N/A", 0.0f) };
	List<StateData> s2ValueList = new List<StateData>() { new StateData("N/A", 0.0f) };
	List<StateData> s3ValueList = new List<StateData>() { new StateData("N/A", 0.0f) };

	int iterationStep = 0;
	string iterationStepStr = "Iteration Step: ";


	// timer for auto run mode
	float runTimer = 0.0f;
	const float NEXT_RUN_TIME = 0.8f;	// in seconds

	bool isAutoRun = false;


	void Update() {
		if(isAutoRun) {
			if( (runTimer += Time.deltaTime) >= NEXT_RUN_TIME) {
				OnOneStep_Click();
				runTimer = 0.0f;
			}
		}
	}


	// This is where we will do the value iteration calculation
	public void OnOneStep_Click() {
		// we first start with state s1
		ValueIterationExe("s1");

		// next state to check is s2
		ValueIterationExe("s2");
		
		// the last state to check is s3
		ValueIterationExe("s3");

		// update the iteration step
		++iterationStep;
		iterationStepText.text = iterationStepStr + iterationStep;
	}

	void ValueIterationExe(string startState) {
		//NOTE:Terminal/absorbing states will have a value that is equal to it's reward. 
		// No matter the number of value itearation steps,
		// terminal/absorbing states value stay the same. 		

		// store the calculation for each action
		float walkValue = 0.0f;
		float teleValue = 0.0f;
		
		// iteration calcuation for each action
		walkValue = rewardCostDic[startState] + (walkTransitionProb[startState]["s1"] * s1ValueList[iterationStep].value) + 
					(walkTransitionProb[startState]["s2"] * s2ValueList[iterationStep].value) + (walkTransitionProb[startState]["s3"] * s3ValueList[iterationStep].value) + 
					(walkTransitionProb[startState]["s4"] * rewardCostDic["s4"]) + (walkTransitionProb[startState]["s5"] * rewardCostDic["s5"]) +
					(walkTransitionProb[startState]["s6"] * rewardCostDic["s6"]) + (walkTransitionProb[startState]["s7"] * rewardCostDic["s7"]);

		teleValue = rewardCostDic[startState] + (teleportTransitionProb[startState]["s1"] * s1ValueList[iterationStep].value) + 
					(teleportTransitionProb[startState]["s2"] * s2ValueList[iterationStep].value) + (teleportTransitionProb[startState]["s3"] * s3ValueList[iterationStep].value) + 
					(teleportTransitionProb[startState]["s4"] * rewardCostDic["s4"]) + (teleportTransitionProb[startState]["s5"] * rewardCostDic["s5"]) +
					(teleportTransitionProb[startState]["s6"] * rewardCostDic["s6"]) + (teleportTransitionProb[startState]["s7"] * rewardCostDic["s7"]);

		// record the new iteration value and policy
		// update the UI
		switch(startState) {
			case "s1":
				if(walkValue > teleValue) {
					s1ValueList.Add(new StateData("WALK", walkValue));
					s1ValueText.text = System.Math.Round((decimal)walkValue, 3, System.MidpointRounding.AwayFromZero).ToString();
					s1PolicyText.text = "WALK";
				}
				else {
					// also if it's equal we will teleport since I like teleporting more than walking
					s1ValueList.Add(new StateData("TELEPORT", teleValue));
					s1ValueText.text = System.Math.Round((decimal)teleValue, 3, System.MidpointRounding.AwayFromZero).ToString();
					s1PolicyText.text = "TELEPORT";
				}				
				break;

			case "s2":
				if(walkValue > teleValue) {
					s2ValueList.Add(new StateData("WALK", walkValue));
					s2ValueText.text = System.Math.Round((decimal)walkValue, 3, System.MidpointRounding.AwayFromZero).ToString();
					s2PolicyText.text = "WALK";
				}
				else {
					// also if it's equal we will teleport since I like teleporting more than walking
					s2ValueList.Add(new StateData("TELEPORT", teleValue));
					s2ValueText.text = System.Math.Round((decimal)teleValue, 3, System.MidpointRounding.AwayFromZero).ToString();
					s2PolicyText.text = "TELEPORT";
				}		
				break;

			case "s3":
				if(walkValue > teleValue) {
					s3ValueList.Add(new StateData("WALK", walkValue));
					s3ValueText.text = System.Math.Round((decimal)walkValue, 3, System.MidpointRounding.AwayFromZero).ToString();
					s3PolicyText.text = "WALK";
				}
				else {
					// also if it's equal we will teleport since I like teleporting more than walking
					s3ValueList.Add(new StateData("TELEPORT", teleValue));
					s3ValueText.text = System.Math.Round((decimal)teleValue, 3, System.MidpointRounding.AwayFromZero).ToString();
					s3PolicyText.text = "TELEPORT";
				}		
				break;
		}	
	}


	public void OnRun_Click() {
		isAutoRun = true;
	}

	public void OnStop_Click() {
		isAutoRun = false;
		runTimer = 0.0f;
	}
}
