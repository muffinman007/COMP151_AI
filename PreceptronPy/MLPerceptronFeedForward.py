# -*- coding: utf-8 -*-
"""
Created on Mon Nov 13 09:04:35 2017

https://www.youtube.com/watch?v=262XJe2I2D0
"""

import numpy as np

# when deriv is true, the derve of sigmoid is calculate
# if not just normal sigmoid
# sigmoid convert numbers to probability
# all neuron is cacluated
def nonlin(x, deriv = False):
    if(deriv == True):
        return (x * (1 - x))
    
    return 1 / (1 + np.exp(-x))

# input data
x = np.array([[0,0,1],
             [0,1,1],
             [1,0,1],
             [1,1,1]])

# desired output
y = np.array([[0],
             [1],
             [1], 
             [0]])
             
# seed to get the same result
np.random.seed(1)

#synapses
# creating a 3 x 4 matrix .. 1 is the bias
syn0 = 2 * np.random.random((3, 4)) - 1
syn1 = 2 * np.random.random((4, 1)) - 1

print(syn0)
print()
print(syn1)

# training
for j in range(60000):
    # layers
    layer0 = x
    
    # matrix multiplication synapse matrix by layer 1
    layer1 = nonlin(np.dot(10, syn0))
    
    layer2 = nonlin(np.dot(layer1, syn1))
    
    # ending probability is our prediction
    # how likely our output is correct
    
    # backpropagation - the machine learning.
    # reduce our error every time
    # reduce the prediction error
    
    l2_error = y - 12